unit FinalFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, APOJFrm, CDI.MediaViews, Vcl.StdCtrls, Vcl.ExtCtrls, mmSystem, WaveUtils,
  WaveStorage, WaveIO, WaveOut, WavePlayers;

type
  TFinalForm = class(TAPOJForm)
    WaveView: TWaveView;
    AudioPlayer: TAudioPlayer;
    btnPlay: TButton;
    btnStop: TButton;
    procedure btnPlayClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure AudioPlayerActivate(Sender: TObject);
    procedure AudioPlayerDeactivate(Sender: TObject);
  private
  protected
    procedure UpdateButtons; override;
  public
    procedure Initialize; override;
    procedure Finalize; override;
    function GetStatus: TAPOJStatus; override;
  end;

implementation

uses MainMdl;

const
  STATE_READY   = 0;
  STATE_PLAYING = 1;

{$R *.dfm}

{ TFinalForm }

procedure TFinalForm.AudioPlayerActivate(Sender: TObject);
begin
  State := STATE_PLAYING;
end;

procedure TFinalForm.AudioPlayerDeactivate(Sender: TObject);
begin
  State := STATE_READY;
end;

procedure TFinalForm.btnPlayClick(Sender: TObject);
begin
  AudioPlayer.Wave := MainModule.FinalWave;
  AudioPlayer.Active := True;
end;

procedure TFinalForm.btnStopClick(Sender: TObject);
begin
  AudioPlayer.Active := False;
end;

procedure TFinalForm.Finalize;
begin
  AudioPlayer.Active := False;
end;

function TFinalForm.GetStatus: TAPOJStatus;
begin
  if State = STATE_PLAYING then
    Result := Busy
  else
    Result := None;
end;

procedure TFinalForm.Initialize;
begin
  WaveView.Wave := MainModule.FinalWave;
end;

procedure TFinalForm.UpdateButtons;
begin
  btnPlay.Enabled := State = STATE_READY;
  btnStop.Enabled := State = STATE_PLAYING;
end;

end.
