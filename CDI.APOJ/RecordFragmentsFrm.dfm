inherited RecordFragmentsForm: TRecordFragmentsForm
  ClientHeight = 497
  ClientWidth = 955
  OnResize = FrameResize
  ExplicitWidth = 955
  ExplicitHeight = 497
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopButtonPanel: TPanel
    Width = 955
    TabOrder = 1
    ExplicitWidth = 955
    inherited btnReset: TButton
      Left = 818
      ExplicitLeft = 818
    end
    object btnPlayFragment: TButton
      Left = 0
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1055#1088#1086#1089#1083#1091#1096#1072#1090#1100' '#1092#1088#1072#1075#1084#1077#1085#1090
      TabOrder = 1
      OnClick = btnPlayFragmentClick
      ExplicitLeft = 8
    end
    object btnRecordFragment: TButton
      Left = 129
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100' '#1092#1088#1072#1075#1084#1077#1085#1090
      TabOrder = 2
      OnClick = btnRecordFragmentClick
      ExplicitLeft = 232
      ExplicitTop = -6
    end
    object btnStopRecord: TButton
      Left = 258
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      TabOrder = 3
      OnClick = btnStopRecordClick
      ExplicitLeft = 376
    end
    object btnNextFragment: TButton
      Left = 387
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1092#1088#1072#1075#1084#1077#1085#1090
      TabOrder = 4
      OnClick = btnNextFragmentClick
      ExplicitLeft = 496
      ExplicitTop = -6
    end
  end
  object FragmentCaptionPanel: TPanel
    Left = 0
    Top = 41
    Width = 955
    Height = 41
    Align = alTop
    Caption = #1060#1088#1072#1075#1084#1077#1085#1090' '#8470'1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitTop = 82
  end
  object FragmentWaveView: TWaveView
    Left = 0
    Top = 82
    Width = 481
    Height = 415
    Align = alLeft
    PixelPerSecond = 50
    ExplicitTop = 123
    ExplicitHeight = 374
  end
  object ReverseWaveView: TWaveView
    Left = 481
    Top = 82
    Width = 474
    Height = 415
    Align = alClient
    PixelPerSecond = 50
    ExplicitTop = 123
    ExplicitHeight = 374
  end
  object ReverseWaveCollection: TWaveCollection
    Waves = <>
    Left = 792
    Top = 160
  end
  object AudioPlayer: TAudioPlayer
    OnActivate = AudioPlayerActivate
    OnDeactivate = AudioPlayerDeactivate
    Left = 192
    Top = 152
  end
  object AudioRecorder: TAudioRecorder
    PCMFormat = Stereo16bit44100Hz
    BufferLength = 100
    Left = 560
    Top = 160
  end
end
