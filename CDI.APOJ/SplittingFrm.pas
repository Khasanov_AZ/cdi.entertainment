unit SplittingFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CDI.MediaViews, Vcl.StdCtrls, Vcl.ExtCtrls, APOJFrm, mmSystem,
  WaveUtils, WaveStorage, WaveIO, WaveOut, WavePlayers;

type
  TSplittingForm = class(TAPOJForm)
    WaveView: TWaveView;
    AudioPlayer: TAudioPlayer;
    btnPlay: TButton;
    btnStop: TButton;
    procedure btnPlayClick(Sender: TObject);
    procedure AudioPlayerDeactivate(Sender: TObject);
    procedure AudioPlayerActivate(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure WaveViewDblClick(Sender: TObject);
  private
  protected
    procedure UpdateButtons; override;
  public
    procedure Initialize; override;
    procedure Finalize; override;
    function GetStatus: TAPOJStatus; override;
  end;

implementation

uses MainMdl;

const
  STATE_READY   = $00;
  STATE_PLAYING = $01;
  STATE_MARKERED = $02;

{$R *.dfm}

procedure TSplittingForm.AudioPlayerActivate(Sender: TObject);
begin
  State := STATE_PLAYING;
end;

procedure TSplittingForm.AudioPlayerDeactivate(Sender: TObject);
begin
  State := STATE_READY;
end;

procedure TSplittingForm.btnPlayClick(Sender: TObject);
begin
  AudioPlayer.Wave := MainModule.ReverseWave;
  AudioPlayer.Active := True;
end;

procedure TSplittingForm.btnStopClick(Sender: TObject);
begin
  AudioPlayer.Active := False;
end;

procedure TSplittingForm.Finalize;
begin
  MainModule.CreateSplittingWaves( WaveView.MarkerArray );
  WaveView.Wave := nil;
end;

function TSplittingForm.GetStatus: TAPOJStatus;
begin
  case State of
    STATE_PLAYING  : Result := Busy;
    STATE_MARKERED : Result := Ready;
    else
      Result := None;
  end;
end;

procedure TSplittingForm.Initialize;
begin
  WaveView.ClearMarkers;
  WaveView.Wave := MainModule.ReverseWave;
  State := STATE_READY;
end;

procedure TSplittingForm.UpdateButtons;
begin
  btnPlay.Enabled := State = STATE_READY;
  btnStop.Enabled := State = STATE_PLAYING;
end;

procedure TSplittingForm.WaveViewDblClick(Sender: TObject);
var
  P: TPoint;
  T: integer;
begin
  P := WaveView.ScreenToClient(Mouse.CursorPos);
  T := WaveView.TimeAt(P.X);
  if T < WaveView.Wave.Length then
  begin
    WaveView.AddMarker(T);
    State := STATE_MARKERED;
  end;
end;

end.
