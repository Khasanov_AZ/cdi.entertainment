inherited RecordForm: TRecordForm
  ClientHeight = 593
  ClientWidth = 956
  ExplicitWidth = 956
  ExplicitHeight = 593
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopButtonPanel: TPanel
    Width = 956
    TabOrder = 1
    ExplicitWidth = 956
    inherited btnReset: TButton
      Left = 819
      ExplicitLeft = 819
    end
    object btnRecord: TButton
      Left = 0
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1047#1072#1087#1080#1089#1100
      TabOrder = 1
      OnClick = btnRecordClick
      ExplicitLeft = -6
    end
    object btnStop: TButton
      Left = 129
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      TabOrder = 2
      OnClick = btnStopClick
      ExplicitLeft = 8
    end
    object btnPlay: TButton
      Left = 258
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1055#1088#1086#1080#1075#1088#1072#1090#1100
      TabOrder = 3
      OnClick = btnPlayClick
      ExplicitLeft = 321
    end
  end
  object AudioPanel: TPanel
    Left = 0
    Top = 41
    Width = 956
    Height = 552
    Align = alClient
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    ExplicitTop = 82
    ExplicitHeight = 511
    object WaveView: TWaveView
      Left = 0
      Top = 0
      Width = 956
      Height = 552
      Align = alClient
      PixelPerSecond = 40
      ExplicitHeight = 511
    end
  end
  object AudioRecorder: TAudioRecorder
    PCMFormat = Stereo16bit44100Hz
    BufferLength = 100
    Left = 216
    Top = 113
  end
  object AudioPlayer: TAudioPlayer
    BufferLength = 100
    OnDeactivate = AudioPlayerDeactivate
    Left = 376
    Top = 113
  end
end
