unit APOJUtils;

interface

uses
  System.Classes;

const
  APOJ_INITIALIZED        = $00;

  APOJ_RECORD             = $10;
  APOJ_RECORD_RECORDING   = $11;
  APOJ_RECORD_RECORDED    = $12;
  APOJ_RECORD_PLAYING     = $13;

  APOJ_SPLITTING          = $20;
  APOJ_SPLITTING_PLAYING  = $21;

  APOJ_FRAGMENTS          = $30;

implementation

end.
