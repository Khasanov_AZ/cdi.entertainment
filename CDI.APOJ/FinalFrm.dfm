inherited FinalForm: TFinalForm
  Caption = 'FinalForm'
  ClientHeight = 496
  ClientWidth = 789
  OldCreateOrder = False
  ExplicitWidth = 789
  ExplicitHeight = 496
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopButtonPanel: TPanel
    Width = 789
    ExplicitWidth = 789
    inherited btnReset: TButton
      Left = 652
      ExplicitLeft = 652
    end
    object btnPlay: TButton
      Left = 0
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1055#1088#1086#1080#1075#1088#1072#1090#1100
      TabOrder = 1
      OnClick = btnPlayClick
      ExplicitLeft = 8
    end
    object btnStop: TButton
      Left = 129
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      TabOrder = 2
      OnClick = btnStopClick
      ExplicitLeft = 232
    end
  end
  object WaveView: TWaveView
    Left = 0
    Top = 41
    Width = 789
    Height = 455
    Align = alClient
    PixelPerSecond = 40
    ExplicitTop = 82
    ExplicitHeight = 414
  end
  object AudioPlayer: TAudioPlayer
    OnActivate = AudioPlayerActivate
    OnDeactivate = AudioPlayerDeactivate
    Left = 528
    Top = 136
  end
end
