unit SelectDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TSelectDialog = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    btnCancel: TButton;
    btnOk: TButton;
    lbFirstPlayer: TListBox;
    lbSecondPlayer: TListBox;
    procedure lbFirstPlayerClick(Sender: TObject);
  private
  public
  end;

  function ShowSelectDialog(ACompetitors: TStringList; var AFirstPlayer, ASecondPlayer: string): boolean;

implementation

function ShowSelectDialog(ACompetitors: TStringList; var AFirstPlayer, ASecondPlayer: string): boolean;
begin
  with TSelectDialog.Create(nil) do
  try
    lbFirstPlayer.Items.Assign(ACompetitors);
    lbSecondPlayer.Items.Assign(ACompetitors);
    Result := ShowModal = mrOk;
    if Result then
    begin
      AFirstPlayer  := lbFirstPlayer.Items[lbFirstPlayer.ItemIndex];
      ASecondPlayer := lbSecondPlayer.Items[lbSecondPlayer.ItemIndex];
    end;
  finally
    Free;
  end;
end;

{$R *.dfm}

procedure TSelectDialog.lbFirstPlayerClick(Sender: TObject);
begin
  btnOK.Enabled := (lbFirstPlayer.ItemIndex <> -1) and (lbSecondPlayer.ItemIndex <> -1)
    and (lbSecondPlayer.ItemIndex <> lbFirstPlayer.ItemIndex);
end;

end.
