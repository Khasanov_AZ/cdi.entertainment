unit APOJFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TAPOJStatus = (None, Ready, Busy);

  TAPOJForm = class(TForm)
    TopButtonPanel: TPanel;
    btnReset: TButton;
    procedure btnResetClick(Sender: TObject);
  private
    FOnChangeState: TNotifyEvent;
    FState: integer;
    procedure SetState(const Value: integer);
  protected
    procedure UpdateButtons; virtual;
    property State: integer read FState write SetState;
  public
    procedure Initialize; virtual;
    procedure Finalize; virtual;
    property OnChangeState: TNotifyEvent read FOnChangeState write FOnChangeState;
    function GetStatus: TAPOJStatus; virtual;
  end;

  TAPOJFormClass = class of TAPOJForm;

implementation

{$R *.dfm}

{ TAPOJFrame }

procedure TAPOJForm.btnResetClick(Sender: TObject);
begin
  Initialize;
end;

procedure TAPOJForm.Finalize;
begin

end;

function TAPOJForm.GetStatus: TAPOJStatus;
begin
  Result := None;
end;

procedure TAPOJForm.Initialize;
begin

end;

procedure TAPOJForm.SetState(const Value: integer);
begin
//  if State <> Value then
  begin
    FState := Value;
    UpdateButtons;
    if Assigned(FOnChangeState) then
      FOnChangeState(Self);
  end;
end;

procedure TAPOJForm.UpdateButtons;
begin

end;

end.
