unit APOJMainFrm;

interface

uses
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  Winapi.Windows, Winapi.Messages, mmSystem,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Graphics, Vcl.ExtCtrls, Vcl.ComCtrls,
  WaveUtils, WaveStorage, WaveIO, WaveIn, WaveRecorders, WaveOut, WavePlayers,
  CDI.MediaViews, APOJUtils, APOJFrm;

type
  TAPOJMainForm = class(TForm)
    CaptionPanel: TPanel;
    CaptionLabel: TLabel;
    MainPanel: TPanel;
    WaveAudioLevelView: TWaveAudioLevelView;
    TopPanel: TPanel;
    btnNewGame: TButton;
    btnReset: TButton;
    btnNext: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnNewGameClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
  private
    FForm: TAPOJForm;
    FForms: TList<TAPOJFormClass>;
    FFormIndex: integer;
    procedure Reset;
    procedure Initialize;
    procedure FormChangeState(Sender: TObject);
    procedure SetFormIndex(const Value: integer);
    property FormIndex: integer read FFormIndex write SetFormIndex;
  public
  end;

var
  APOJMainForm: TAPOJMainForm;

implementation

uses SelectDlg, MainMdl, RecordFrm, SplittingFrm, RecordFragmentsFrm, FinalFrm;

{$R *.dfm}

procedure TAPOJMainForm.btnNewGameClick(Sender: TObject);
var
  Player1, Player2: string;
begin
  if not ShowSelectDialog(MainModule.Competitors, Player1, Player2) then
    Exit;

  MainModule.StartGame(Player1, Player2);
  CaptionLabel.Caption := Player1 + ' � ' + Player2;
  FormIndex := 0;
end;

procedure TAPOJMainForm.btnNextClick(Sender: TObject);
begin
  if FormIndex < FForms.Count - 1 then
    FormIndex := FormIndex + 1;
end;

procedure TAPOJMainForm.btnResetClick(Sender: TObject);
begin
  if MessageDlg('������ �������� ����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    Reset;
end;

procedure TAPOJMainForm.FormChangeState(Sender: TObject);
var
  Status: TAPOJStatus;
begin
  Status := TAPOJForm(Sender).GetStatus;
  btnNewGame.Enabled := Status in [None, Ready];
  btnReset.Enabled   := Status in [None, Ready];
  btnNext.Enabled    := Status = Ready;
end;

procedure TAPOJMainForm.FormCreate(Sender: TObject);
begin
  FForms := TList<TAPOJFormClass>.Create;
  FForms.Add( TRecordForm );
  FForms.Add( TSplittingForm );
  FForms.Add( TRecordFragmentsForm );
  FForms.Add( TFinalForm );
  Reset;
end;

procedure TAPOJMainForm.Initialize;
begin
  FormIndex := -1;
end;

procedure TAPOJMainForm.Reset;
begin
  MainModule.FinalizeGame;
  Initialize;
end;

procedure TAPOJMainForm.SetFormIndex(const Value: integer);
begin
  btnNewGame.Enabled := Value = -1;
  btnNext.Enabled    := Value < FForms.Count - 1;

  if FForm <> nil then
    FForm.Finalize;

  FreeAndNil(FForm);

  FFormIndex := Value;
  if FormIndex = -1 then
    Exit;

  FForm := FForms[FFormIndex].Create(Self);
  FForm.Parent := MainPanel;
  FForm.Align := alClient;
  FForm.BorderStyle := bsNone;
  FForm.Show;
  FForm.OnChangeState := FormChangeState;
  FForm.Initialize;
end;

end.
