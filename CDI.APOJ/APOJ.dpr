program APOJ;

uses
  Vcl.Forms,
  APOJMainFrm in 'APOJMainFrm.pas' {APOJMainForm},
  MainMdl in 'MainMdl.pas' {MainModule: TDataModule},
  SelectDlg in 'SelectDlg.pas' {SelectDialog},
  APOJUtils in 'APOJUtils.pas',
  RecordFragmentsFrm in 'RecordFragmentsFrm.pas' {RecordFragmentsForm: TFrame},
  RecordFrm in 'RecordFrm.pas' {RecordForm: TFrame},
  APOJFrm in 'APOJFrm.pas' {APOJFrame: TFrame},
  SplittingFrm in 'SplittingFrm.pas' {SplittingForm: TFrame},
  FinalFrm in 'FinalFrm.pas' {FinalForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainModule, MainModule);
  Application.CreateForm(TAPOJMainForm, APOJMainForm);
  Application.Run;
end.
