object MainModule: TMainModule
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 361
  Width = 604
  object LiveAudioRecorder: TLiveAudioRecorder
    PCMFormat = Stereo16bit44100Hz
    BufferLength = 100
    Left = 48
    Top = 8
  end
  object WaveCollection: TWaveCollection
    Waves = <>
    Left = 200
    Top = 16
  end
end
