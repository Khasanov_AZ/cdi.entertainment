object SelectDialog: TSelectDialog
  Left = 0
  Top = 0
  Caption = 'SelectDialog'
  ClientHeight = 402
  ClientWidth = 535
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    535
    402)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 133
    Height = 19
    Caption = #1055#1077#1088#1074#1099#1081' '#1091#1095#1072#1089#1090#1085#1080#1082':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 271
    Top = 16
    Width = 130
    Height = 19
    Caption = #1042#1090#1086#1088#1086#1081' '#1091#1095#1072#1089#1090#1085#1080#1082':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btnCancel: TButton
    Left = 397
    Top = 344
    Width = 130
    Height = 50
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1058#1052#1045#1053#1040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 305
    ExplicitTop = 143
  end
  object btnOk: TButton
    Left = 261
    Top = 344
    Width = 130
    Height = 50
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = 169
    ExplicitTop = 143
  end
  object lbFirstPlayer: TListBox
    Left = 8
    Top = 41
    Width = 257
    Height = 296
    Anchors = [akLeft, akTop, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 19
    ParentFont = False
    Sorted = True
    TabOrder = 2
    OnClick = lbFirstPlayerClick
  end
  object lbSecondPlayer: TListBox
    Left = 271
    Top = 41
    Width = 257
    Height = 296
    Anchors = [akLeft, akTop, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 19
    ParentFont = False
    Sorted = True
    TabOrder = 3
    OnClick = lbFirstPlayerClick
  end
end
