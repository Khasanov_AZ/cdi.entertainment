unit MainMdl;

interface

uses
  System.SysUtils, System.Classes, Data.DB, MemDS, VirtualTable, Vcl.Forms, mmSystem, WaveUtils, WaveStorage, WaveIO,
  WaveOut, WavePlayers, WaveRecorders, WaveIn;

type
  TCallbackProc = procedure of object;

  TMainModule = class(TDataModule)
    LiveAudioRecorder: TLiveAudioRecorder;
    WaveCollection: TWaveCollection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FCompetitors: TStringList;
    FFirstPlayer, FSecondPlayer: string;
    FEndPlayCallbackProc: TCallbackProc;
    FOriginalWave: TWaveStreamAdapter;
    FReverseWave: TWaveStreamAdapter;
    FFinalWave: TWaveStreamAdapter;
    function GetAppPath: string;
    function GetCompetitorsPath: string;
    procedure SetOriginalWave(const Value: TWaveStreamAdapter);
    function GetFragment(AIndex: integer): TWaveStreamAdapter;
    procedure SetFinalWave(const Value: TWaveStreamAdapter);
  public
    property Competitors: TStringList read FCompetitors;
    procedure InitializeGame;
    procedure FinalizeGame;
    procedure StartGame(AFirstPlayer, ASecondPlayer: string);
    procedure SaveWave(AWave: TWaveStreamAdapter; AFileName: string);
    procedure CreateSplittingWaves(Markers: TArray<integer>);
    property FirstPlayer: string read FFirstPlayer;
    property SecondPlayer: string read FSecondPlayer;
    property AppPath: string read GetAppPath;
    property CompetitorsPath: string read GetCompetitorsPath;
    property OriginalWave: TWaveStreamAdapter read FOriginalWave write SetOriginalWave;
    property ReverseWave: TWaveStreamAdapter read FReverseWave;
    property FinalWave: TWaveStreamAdapter read FFinalWave write SetFinalWave;
    property Fragments[AIndex: integer]: TWaveStreamAdapter read GetFragment;
  end;

var
  MainModule: TMainModule;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TMainModule.CreateSplittingWaves(Markers: TArray<integer>);
var
  I, Start, Finish: integer;
  WaveItem: TWaveItem;
begin
  WaveCollection.Waves.Clear;
  Start := 0;
  for I := 0 to Length(Markers) - 1 do
  begin
    WaveItem := WaveCollection.Waves.Add;
    WaveItem.Wave.Copy(ReverseWave, Start, Markers[I] - Start);
    Start := Markers[I];
  end;
  WaveItem := WaveCollection.Waves.Add;
  WaveItem.Wave.Copy(ReverseWave, Start, ReverseWave.Length - Start);
end;

procedure TMainModule.DataModuleCreate(Sender: TObject);
var
  FileName: string;
begin
  FCompetitors := TStringList.Create;
  FileName := AppPath + 'competitors.txt';
  if FileExists(FileName) then
    FCompetitors.LoadFromFile( FileName, TEncoding.UTF8 );

  FOriginalWave := TWave.Create;
  FReverseWave  := TWave.Create;
  FFinalWave    := TWave.Create;
end;

procedure TMainModule.DataModuleDestroy(Sender: TObject);
begin
  FCompetitors.Free;
  FOriginalWave.Free;
  FReverseWave.Free;
  FFinalWave.Free;
end;

procedure TMainModule.FinalizeGame;
begin

end;

function TMainModule.GetAppPath: string;
begin
  Result := ExtractFilePath( Application.ExeName );
end;

function TMainModule.GetCompetitorsPath: string;
begin
  if (FFirstPlayer <> '') and (FSecondPlayer <> '') then
    Result := AppPath + FFirstPlayer + ' � ' + FSecondPlayer
  else
    Result := '������ ����������';
end;

function TMainModule.GetFragment(AIndex: integer): TWaveStreamAdapter;
begin
  Result := WaveCollection.Waves[AIndex].Wave;
end;

procedure TMainModule.InitializeGame;
begin
  LiveAudioRecorder.Active := False;
end;

procedure TMainModule.SaveWave(AWave: TWaveStreamAdapter; AFileName: string);
begin
  if not DirectoryExists(CompetitorsPath) then
    ForceDirectories(CompetitorsPath);
  AWave.SaveToFile(CompetitorsPath + '\' + AFileName + '.wav');
end;

procedure TMainModule.SetOriginalWave(const Value: TWaveStreamAdapter);
begin
  SaveWave(Value, 'ORIGINAL');

  Value.Stream.Position := 0;
  FOriginalWave.LoadFromStream(Value.Stream);

  Value.Stream.Position := 0;
  FReverseWave.LoadFromStream(Value.Stream);
  FReverseWave.Invert;
end;

procedure TMainModule.SetFinalWave(const Value: TWaveStreamAdapter);
begin
  SaveWave(Value, 'READY');

  Value.Stream.Position := 0;
  FFinalWave.LoadFromStream(Value.Stream);
end;

procedure TMainModule.StartGame(AFirstPlayer, ASecondPlayer: string);
begin
  FFirstPlayer := AFirstPlayer;
  FSecondPlayer := ASecondPlayer;
  LiveAudioRecorder.Active := True;
end;

end.
