inherited SplittingForm: TSplittingForm
  ClientHeight = 448
  ClientWidth = 850
  ExplicitWidth = 850
  ExplicitHeight = 448
  PixelsPerInch = 96
  TextHeight = 13
  inherited TopButtonPanel: TPanel
    Width = 850
    ExplicitWidth = 850
    inherited btnReset: TButton
      Left = 713
      ExplicitLeft = 713
    end
    object btnPlay: TButton
      Left = 0
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1055#1088#1086#1080#1075#1088#1072#1090#1100
      TabOrder = 1
      OnClick = btnPlayClick
    end
    object btnStop: TButton
      Left = 129
      Top = 0
      Width = 129
      Height = 41
      Align = alLeft
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      TabOrder = 2
      OnClick = btnStopClick
    end
  end
  object WaveView: TWaveView
    Left = 0
    Top = 41
    Width = 850
    Height = 407
    Align = alClient
    PixelPerSecond = 40
    OnDblClick = WaveViewDblClick
  end
  object AudioPlayer: TAudioPlayer
    OnActivate = AudioPlayerActivate
    OnDeactivate = AudioPlayerDeactivate
    Left = 520
    Top = 152
  end
end
