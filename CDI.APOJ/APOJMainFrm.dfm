object APOJMainForm: TAPOJMainForm
  Left = 0
  Top = 0
  Caption = 'APOJMainForm'
  ClientHeight = 623
  ClientWidth = 1056
  Color = clAppWorkSpace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object CaptionPanel: TPanel
    Left = 0
    Top = 41
    Width = 1056
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object CaptionLabel: TLabel
      Left = 0
      Top = 0
      Width = 1056
      Height = 41
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Color = 6502166
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 13410946
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      Layout = tlCenter
      ExplicitLeft = 240
      ExplicitTop = 8
      ExplicitWidth = 31
      ExplicitHeight = 13
    end
  end
  object MainPanel: TPanel
    Left = 0
    Top = 82
    Width = 1056
    Height = 541
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 56
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object WaveAudioLevelView: TWaveAudioLevelView
      Left = 0
      Top = 0
      Width = 32
      Height = 541
      WaveAudio = MainModule.LiveAudioRecorder
      Align = alLeft
      Level = 0
    end
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 1056
    Height = 41
    Align = alTop
    TabOrder = 2
    object btnNewGame: TButton
      Left = 1
      Top = 1
      Width = 129
      Height = 39
      Align = alLeft
      Caption = #1053#1086#1074#1072#1103' '#1080#1075#1088#1072
      TabOrder = 0
      OnClick = btnNewGameClick
    end
    object btnReset: TButton
      Left = 926
      Top = 1
      Width = 129
      Height = 39
      Align = alRight
      Caption = #1057#1073#1088#1086#1089
      TabOrder = 1
      OnClick = btnResetClick
    end
    object btnNext: TButton
      Left = 130
      Top = 1
      Width = 129
      Height = 39
      Align = alLeft
      Caption = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1101#1090#1072#1087
      TabOrder = 2
      OnClick = btnNextClick
    end
  end
end
