﻿unit RecordFragmentsFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, mmSystem, WaveUtils, WaveStorage, CDI.MediaViews, Vcl.StdCtrls,
  Vcl.ExtCtrls, APOJFrm, WaveIO, WaveOut, WavePlayers, WaveIn, WaveRecorders;

type
  TRecordFragmentsForm = class(TAPOJForm)
    FragmentCaptionPanel: TPanel;
    ReverseWaveCollection: TWaveCollection;
    AudioPlayer: TAudioPlayer;
    FragmentWaveView: TWaveView;
    ReverseWaveView: TWaveView;
    AudioRecorder: TAudioRecorder;
    btnPlayFragment: TButton;
    btnRecordFragment: TButton;
    btnStopRecord: TButton;
    btnNextFragment: TButton;
    procedure FrameResize(Sender: TObject);
    procedure btnPlayFragmentClick(Sender: TObject);
    procedure AudioPlayerDeactivate(Sender: TObject);
    procedure btnNextFragmentClick(Sender: TObject);
    procedure btnRecordFragmentClick(Sender: TObject);
    procedure btnStopRecordClick(Sender: TObject);
    procedure AudioPlayerActivate(Sender: TObject);
  private
    FFragmentIndex: integer;
    WaveItem: TWaveItem;
    procedure SetFragmentIndex(const Value: integer);
    procedure UpdateButtons; override;
    function GetFragmentCount: integer;
  public
    procedure Initialize; override;
    procedure Finalize; override;
    property FragmentIndex: integer read FFragmentIndex write SetFragmentIndex;
    property FragmentCount: integer read GetFragmentCount;
    function GetStatus: TAPOJStatus; override;
  end;

implementation

uses MainMdl;

const
  STATE_READY     = $00;
  STATE_PLAYING   = $01;
  STATE_RECORDING = $02;

{$R *.dfm}

{ TRecordFragmentsFrame }

procedure TRecordFragmentsForm.AudioPlayerActivate(Sender: TObject);
begin
  State := STATE_PLAYING;
end;

procedure TRecordFragmentsForm.AudioPlayerDeactivate(Sender: TObject);
begin
  State := STATE_READY;
end;

procedure TRecordFragmentsForm.btnNextFragmentClick(Sender: TObject);
begin
  FragmentIndex := FragmentIndex + 1;
end;

procedure TRecordFragmentsForm.btnPlayFragmentClick(Sender: TObject);
begin
  AudioPlayer.Active := True;
end;

procedure TRecordFragmentsForm.btnRecordFragmentClick(Sender: TObject);
begin
  AudioRecorder.Active := True;
  State := STATE_RECORDING;
end;

procedure TRecordFragmentsForm.btnStopRecordClick(Sender: TObject);
var
  WaveItem: TWaveItem;
begin
  AudioRecorder.Active := False;
  AudioRecorder.WaitForStop;
  ReverseWaveCollection.Waves[FragmentIndex].Wave := TWave(AudioRecorder.Wave);
  State := STATE_READY;
end;

procedure TRecordFragmentsForm.Finalize;
var
  Wave: TWave;
  Waves: array of TWaveStreamAdapter;
  I, Start: Integer;
begin
  Wave := TWave.Create;
  Waves := [];
  for I := 0 to ReverseWaveCollection.Waves.Count - 1 do
    Waves := Waves + [ReverseWaveCollection.Waves[I].Wave];
  Wave.Concat(Waves);
  Wave.Invert;
  MainModule.FinalWave := Wave;
end;

procedure TRecordFragmentsForm.FrameResize(Sender: TObject);
begin
  FragmentWaveView.Width := ClientWidth div 2;
end;

function TRecordFragmentsForm.GetFragmentCount: integer;
begin
  Result := MainModule.WaveCollection.Waves.Count;
end;

function TRecordFragmentsForm.GetStatus: TAPOJStatus;
begin
  if State in [STATE_RECORDING, STATE_PLAYING] then
    Result := Busy
  else if FFragmentIndex = MainModule.WaveCollection.Waves.Count - 1 then
    Result := Ready
  else
    Result := None;
end;

procedure TRecordFragmentsForm.Initialize;
var
  I: Integer;
begin
  FragmentIndex := 0;
  ReverseWaveView.Wave := AudioRecorder.Wave;
  ReverseWaveCollection.Waves.Clear;
  for I := 0 to FragmentCount - 1 do
    ReverseWaveCollection.Waves.Add;
  State := STATE_READY;
end;

procedure TRecordFragmentsForm.SetFragmentIndex(const Value: integer);
begin
  FFragmentIndex := Value;
  AudioRecorder.Wave.Clear;
  ReverseWaveView.Repaint;
  FragmentCaptionPanel.Caption := 'Фрагмент №' + (FFragmentIndex + 1).ToString;
  FragmentWaveView.Wave := MainModule.Fragments[FragmentIndex];
//  ReverseWaveView.Wave := ReverseWaveCollection.Waves[FragmentIndex].Wave;
  AudioPlayer.Wave := MainModule.Fragments[FragmentIndex];
  UpdateButtons;
end;

procedure TRecordFragmentsForm.UpdateButtons;
begin
  btnPlayFragment.Enabled   := State = STATE_READY;
  btnRecordFragment.Enabled := State = STATE_READY;
  btnStopRecord.Enabled     := State = STATE_RECORDING;
  btnNextFragment.Enabled   := (State = STATE_READY) and (FFragmentIndex < FragmentCount - 1);
end;

end.
