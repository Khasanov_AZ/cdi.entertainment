unit RecordFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  APOJFrm, CDI.MediaViews, Vcl.StdCtrls, Vcl.ExtCtrls, mmSystem, WaveUtils, WaveStorage, WaveIO, WaveIn, WaveRecorders,
  WaveOut, WavePlayers;

type
  TRecordForm = class(TAPOJForm)
    AudioPanel: TPanel;
    WaveView: TWaveView;
    AudioRecorder: TAudioRecorder;
    AudioPlayer: TAudioPlayer;
    btnRecord: TButton;
    btnStop: TButton;
    btnPlay: TButton;
    procedure btnRecordClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure btnPlayClick(Sender: TObject);
    procedure AudioPlayerDeactivate(Sender: TObject);
  private
  protected
    procedure UpdateButtons; override;
  public
    procedure Initialize; override;
    procedure Finalize; override;
    function GetStatus: TAPOJStatus; override;
  end;

implementation

uses SelectDlg, MainMdl;

const
  STATE_READY     = $00;
  STATE_RECORDING = $01;
  STATE_RECORDED  = $02;
  STATE_PLAYING   = $03;

{$R *.dfm}

procedure TRecordForm.AudioPlayerDeactivate(Sender: TObject);
begin
  State := STATE_RECORDED;
end;

procedure TRecordForm.btnPlayClick(Sender: TObject);
begin
  AudioPlayer.Wave := AudioRecorder.Wave;
  AudioPlayer.Active := True;
  State := STATE_PLAYING;
end;

procedure TRecordForm.btnRecordClick(Sender: TObject);
begin
  AudioRecorder.Active := True;
  State := STATE_RECORDING;
end;

procedure TRecordForm.btnStopClick(Sender: TObject);
begin
  if State = STATE_RECORDING then
  begin
    AudioRecorder.Active := False;
    State := STATE_RECORDED;
  end
  else begin
    AudioPlayer.Active := False;
  end;
end;

procedure TRecordForm.Finalize;
begin
  MainModule.OriginalWave := AudioRecorder.Wave;
  WaveView.Wave := nil;
end;

function TRecordForm.GetStatus: TAPOJStatus;
begin
  case State of
    STATE_READY: Result := None;
    STATE_RECORDED: Result := Ready;
    else
      Result := Busy;
  end;
end;

procedure TRecordForm.Initialize;
begin
  AudioRecorder.Wave.Clear;
  WaveView.Wave := AudioRecorder.Wave;
  State := STATE_READY;
end;

procedure TRecordForm.UpdateButtons;
begin
  btnRecord.Enabled := State = STATE_READY;
  btnStop.Enabled   := (State = STATE_RECORDING) or (State = STATE_PLAYING);
  btnPlay.Enabled   := State = STATE_RECORDED;
end;

end.
