object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Test Project'
  ClientHeight = 643
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnStartRecord: TButton
    Left = 160
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Start Record'
    TabOrder = 0
    OnClick = btnStartRecClick
  end
  object btnStopRecord: TButton
    Left = 257
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Stop Record'
    TabOrder = 1
    OnClick = btnStopRecClick
  end
  object btnStopPlay: TButton
    Left = 257
    Top = 521
    Width = 75
    Height = 25
    Caption = 'Stop Play'
    TabOrder = 2
    OnClick = btnStopClick
  end
  object btnStartPlay: TButton
    Left = 159
    Top = 521
    Width = 75
    Height = 25
    Caption = 'Start Play'
    TabOrder = 3
    OnClick = btnPlayClick
  end
  object Button1: TButton
    Left = 472
    Top = 512
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 5
    OnClick = Button1Click
  end
  object btnReverse: TButton
    Left = 338
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Reverse'
    TabOrder = 4
    OnClick = btnReverseClick
  end
  object btnSave: TButton
    Left = 419
    Top = 481
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 6
    OnClick = btnSaveClick
  end
  object AudioRecorder: TAudioRecorder
    Left = 232
    Top = 384
  end
  object Timer1: TTimer
    Left = 512
    Top = 344
  end
end
