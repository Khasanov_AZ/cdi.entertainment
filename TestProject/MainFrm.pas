unit MainFrm;

interface

uses
  System.SysUtils, System.Variants, System.Classes,
  Winapi.Windows, Winapi.Messages,
  Vcl.Graphics, Vcl.Menus, Vcl.StdCtrls, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls,
  CDI.Devices, CDI.MediaViews, CDI.Devices.Microphone, CDI.AudioBuffer, CDI.Devices.AudioPlayer;

type
  TMainForm = class(TForm)
    Microphone: TMicrophone;
    MicrophoneLevelView: TMicrophoneLevelView;
    AudioRecorder: TAudioRecorder;
    AudioBuffer: TAudioBuffer;
    btnStartRecord: TButton;
    btnStopRecord: TButton;
    btnStopPlay: TButton;
    btnStartPlay: TButton;
    AudioBufferView: TAudioBufferView;
    btnReverse: TButton;
    btnSave: TButton;
    Timer1: TTimer;
    procedure btnStartRecClick(Sender: TObject);
    procedure btnStopRecClick(Sender: TObject);
    procedure btnPlayClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnReverseClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
  public
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btnPlayClick(Sender: TObject);
begin  //AudioPlayer.Start;
end;

procedure TMainForm.btnReverseClick(Sender: TObject);begin
  AudioBuffer.Reverse;
end;

procedure TMainForm.btnSaveClick(Sender: TObject);begin
  AudioBuffer.SaveToFile('D:\Sample.wav', True);
end;

procedure TMainForm.btnStartRecClick(Sender: TObject);begin  AudioRecorder.Start;
end;

procedure TMainForm.btnStopClick(Sender: TObject);
begin//  AudioPlayer.Stop;
end;

procedure TMainForm.btnStopRecClick(Sender: TObject);
begin  AudioRecorder.Stop;

  AudioBuffer.SaveToFile('d:\sample.wav', True);end;

procedure TMainForm.Button1Click(Sender: TObject);
var
  Generator: TSinusAudioGenerator;
begin  Generator := TSinusAudioGenerator.Create;  Generator.AudioBuffer := AudioBuffer;  Generator.Level := 30000;  Generator.Frequency := 330;  Generator.Generate;  AudioBuffer.SaveToFile('d:\sinus330.wav', True);
end;

end.
