unit CDI.AudioBuffer;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections, System.Types, System.Math,
  Winapi.Windows, Winapi.Messages, Winapi.MMSystem,
  CDI.Devices, CDI.Devices.Microphone;

type
  TAudioBuffer = class;
  TAudioGenerator = class;
  TAudioGeneratorClass = class of TAudioGenerator;

  TWAVHeader = packed record
    RIFFMarker: DWORD;
    ChunkSize: Longint;
    WAVEHeader: DWORD;
    FMTMarker: DWORD;
    SubChunkSize: Cardinal;
    FormatTag: Word;
    NumChannels: Word;
    SampleRate: Longint;
    BytesPerSecond: Longint;
    BlockAlign: Word;
    BitsPerSample: Word;
    DataMarker: DWORD;
    DataBytes: Longint;
  public
    constructor Create(ADataSize: Longint; AProperties: TCDIAudioProperties);
  end;

  TAudioBufferNotifyEvent = procedure(ABuffer: TAudioBuffer) of object;

  TAudioBuffer = class(TComponent)
  private
    FSamples: array of TAudioSample;
    FSampleCount: integer;
    FBufferLength: integer;
    FNotifyEvents: TList<TAudioBufferNotifyEvent>;
    function GetSample(AIndex: integer): TAudioSample;
    procedure SetBufferLength(const Value: integer);
    function GetPBuffer: Pointer;
    procedure Notify;
    function GetBufferSize: Longint;
  protected
    function AddBufferData(ABuffer: PAudioSamples; ABufferSize: integer): boolean;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SaveToFile(AFileName: string; AWAVFormat: boolean = False);
    procedure LoadFromFile(AFileName: string);
    procedure AddNotifyEvent(ANotifyEvent: TAudioBufferNotifyEvent);
    procedure RemoveNotifyEvent(ANotifyEvent: TAudioBufferNotifyEvent);
    procedure Reverse;
    property PBuffer: Pointer read GetPBuffer;
    property Samples[AIndex: integer]: TAudioSample read GetSample; default;
    property SampleCount: integer read FSampleCount;
//    property Channels[AChannel, AIndex: integer]: TAudioSample read GetChannel;
    property BufferSize: Longint read GetBufferSize;
  published
    property BufferLength: integer read FBufferLength write SetBufferLength;
  end;

  TAudioRecorderReached = (arRaise, arStop, arCycle);

  TAudioRecorder = class(TComponent, IMicrophoneListener)
  private
    FMicrophone: TMicrophone;
    FAudioBuffer: TAudioBuffer;
    FRecording: boolean;
    procedure SetMicrophone(const Value: TMicrophone);
    procedure SetAudioBuffer(const Value: TAudioBuffer);
    procedure SetRecording(const Value: boolean);
  protected
    procedure MicrophoneNotify(AMicrophone: TMicrophone; ANotifyType: TMicrophoneNotifyType;
      ABuffer: PAudioSamples; ABufferSize: integer);
  public
    procedure Start;
    procedure Stop;
    procedure Pause;
  published
    property AudioBuffer: TAudioBuffer read FAudioBuffer write SetAudioBuffer;
    property Microphone: TMicrophone read FMicrophone write SetMicrophone;
    property Recording: boolean read FRecording write SetRecording;
  end;

  TAudioGenerator = class
  private
    FAudioBuffer: TAudioBuffer;
    FLevel: TAudioSample;
  protected
    function InternalGenerate(I: integer): TAudioSample; virtual;
    function SampleRate: integer; inline;
  public
    procedure Generate;
    property AudioBuffer: TAudioBuffer read FAudioBuffer write FAudioBuffer;
    property Level: TAudioSample read FLevel write FLevel;
  end;

  TSinusAudioGenerator = class(TAudioGenerator)
  private
    FFrequency: single;
  protected
    function InternalGenerate(I: integer): TAudioSample; override;
  public
    property Frequency: single read FFrequency write FFrequency;
  end;

  TWhitenoiseAudioGenerator = class(TAudioGenerator)
  protected
    function InternalGenerate(I: integer): TAudioSample; override;
  end;

  TMeandrAudioGenerator = class(TAudioGenerator)
  private
    FFrequency: single;
  protected
    function InternalGenerate(I: integer): TAudioSample; override;
  public
    property Frequency: single read FFrequency write FFrequency;
  end;

  TEnvelopeAudioGenerator = class(TAudioGenerator)
  private
    FFrequency: single;
    FEnvelope: single;
  protected
    function InternalGenerate(I: integer): TAudioSample; override;
  public
    property Frequency: single read FFrequency write FFrequency;
    property Envelope: single read FEnvelope write FEnvelope;
  end;

  procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('CDI', [TAudioBuffer, TAudioRecorder]);
end;

{ TAudioBuffer }

function TAudioBuffer.AddBufferData(ABuffer: PAudioSamples; ABufferSize: integer): boolean;
var
  I: Integer;
begin
  I := 0;
  while (I < ABufferSize div 2) and (FSampleCount + I < BufferLength) do
  begin
    FSamples[FSampleCount + I] := ABuffer^[I];
    inc(I);
  end;
  FSampleCount := FSampleCount + I;
  Result := FSampleCount < BufferLength;

  Notify;
end;

procedure TAudioBuffer.AddNotifyEvent(ANotifyEvent: TAudioBufferNotifyEvent);
begin
  FNotifyEvents.Add(ANotifyEvent);
end;

constructor TAudioBuffer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FNotifyEvents := TList<TAudioBufferNotifyEvent>.Create;
  BufferLength := 1000000;
end;

destructor TAudioBuffer.Destroy;
begin
  SetLength(FSamples, 0);
  FNotifyEvents.Free;
  inherited Destroy;
end;

function TAudioBuffer.GetBufferSize: Longint;
begin
  Result := SampleCount * SizeOf(TAudioSample);
end;

function TAudioBuffer.GetPBuffer: Pointer;
begin
 Result := @FSamples;
end;

function TAudioBuffer.GetSample(AIndex: integer): TAudioSample;
begin
  Result := FSamples[AIndex];
end;

procedure TAudioBuffer.LoadFromFile(AFileName: string);
var
  F: File;
  I: Integer;
begin
  AssignFile(F, AFileName);
  Reset(F);
  try
    BufferLength := FileSize(F) div SizeOf(TAudioSample);
//    for I := 0 to BufferLength - 1 do
      BlockRead(F, FSamples, FileSize(F));
    FSampleCount := BufferLength;
  finally
    CloseFile(F);
  end;
end;

procedure TAudioBuffer.Notify;
var
  NotifyEvent: TAudioBufferNotifyEvent;
begin
  for NotifyEvent in FNotifyEvents do
    NotifyEvent(Self);
end;

procedure TAudioBuffer.RemoveNotifyEvent(ANotifyEvent: TAudioBufferNotifyEvent);
begin
  FNotifyEvents.Remove(ANotifyEvent);
end;

procedure TAudioBuffer.Reverse;
var
  I: Integer;
  S: TAudioSample;
begin
  for I := 0 to SampleCount div 2 - 1 do
  begin
    S := FSamples[I];
    FSamples[I] := FSamples[SampleCount - I];
    FSamples[SampleCount - I] := S;
  end;

  Notify;
end;

procedure TAudioBuffer.SaveToFile(AFileName: string; AWAVFormat: boolean);
var
  I: Integer;
  WAVHeader: TWAVHeader;
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(AFileName, fmCreate or fmOpenWrite);

  if AWAVFormat then
  begin
    WAVHeader := TWAVHeader.Create(BufferSize, TCDIAudioProperties.DefaultProperties );
    Stream.WriteData(WAVHeader, SizeOf(WAVHeader));
  end;

  try
    for I := 0 to SampleCount - 1 do
      Stream.WriteData(FSamples[I], SizeOf(TAudioSample));
  finally
    Stream.Free;
  end;
end;

procedure TAudioBuffer.SetBufferLength(const Value: integer);
begin
  if BufferLength <> Value then
  begin
    FBufferLength := Value;
    SetLength(FSamples, Value);
    if SampleCount > BufferLength then
      FSampleCount := BufferLength;
  end;
end;

{ TAudioRecorder }

procedure TAudioRecorder.MicrophoneNotify(AMicrophone: TMicrophone; ANotifyType: TMicrophoneNotifyType;
  ABuffer: PAudioSamples; ABufferSize: integer);
begin
  if Recording and (AudioBuffer <> nil) then
    AudioBuffer.AddBufferData(ABuffer, ABufferSize);
end;

procedure TAudioRecorder.Pause;
begin
  FRecording := False;
end;

procedure TAudioRecorder.SetAudioBuffer(const Value: TAudioBuffer);
begin
  if (AudioBuffer <> Value) and not Recording then
    FAudioBuffer := Value;
end;

procedure TAudioRecorder.SetMicrophone(const Value: TMicrophone);
begin
  if Microphone <> Value then
  begin
    if Microphone <> nil then
      Microphone.RemoveListener(Self);

    FMicrophone := Value;

    if Microphone <> nil then
      Microphone.AddListener(Self);
  end;
end;

procedure TAudioRecorder.SetRecording(const Value: boolean);
begin
  if Recording <> Value then
  begin
    if Value then
      Start
    else
      Stop;
  end;
end;

procedure TAudioRecorder.Start;
begin
  if Microphone <> nil then
  begin
    Microphone.Active := True;
    FRecording := True;
  end;
end;

procedure TAudioRecorder.Stop;
begin
  FRecording := False;
end;

{ TWaveHeader }

constructor TWAVHeader.Create(ADataSize: Longint; AProperties: TCDIAudioProperties);
begin
  RIFFMarker     := $46464952; // "RIFF"
  ChunkSize      := ADataSize + 36;
  WAVEHeader     := $45564157; // "WAVE"
  FMTMarker      := $20746D66; // "fmt "
  SubChunkSize   := 16;
  FormatTag      := 1;  // No process (compress etc)
  NumChannels    := AProperties.Channels;
  SampleRate     := AProperties.SampleRate;
  BytesPerSecond := AProperties.BytesRate;
  BlockAlign     := AProperties.BlockAlign;
  BitsPerSample  := AProperties.BitsPerSample;
  DataMarker     := $61746164; // "data"
  DataBytes      := ADataSize;
end;

{ TAudioGenerator }

procedure TAudioGenerator.Generate;
var
  I: Integer;
  Sample: TAudioSample;
begin
  if AudioBuffer = nil then
    Exit;
  if Level = 0 then
    Exit;

  for I := 0 to AudioBuffer.BufferLength div 2 - 1 do
  begin
    Sample := InternalGenerate(I);
    AudioBuffer.FSamples[2 * I]     := Sample;
    AudioBuffer.FSamples[2 * I + 1] := Sample;
  end;

  AudioBuffer.FSampleCount := AudioBuffer.BufferLength;
end;

function TAudioGenerator.InternalGenerate(I: Integer): TAudioSample;
begin
  Result := 0;
end;

function TAudioGenerator.SampleRate: integer;
begin
  Result := TCDIAudioProperties.DefaultProperties.SampleRate;
end;

{ TSinusAudioGenerator }

function TSinusAudioGenerator.InternalGenerate(I: Integer): TAudioSample;
begin
  Result := Round(Level * sin( 2 * Pi * I * Frequency / SampleRate ));
end;

{ TWhitenoiseAudioGenerator }

function TWhitenoiseAudioGenerator.InternalGenerate(I: Integer): TAudioSample;
begin
  Result := Random( Level * 2 ) - Level;
end;

{ TMeandrAudioGenerator }

function TMeandrAudioGenerator.InternalGenerate(I: Integer): TAudioSample;
begin
  Result := Level * Sign( sin( 2 * Pi * I * Frequency / SampleRate ));
end;

{ TEnvelopeAudioGenerator }

function TEnvelopeAudioGenerator.InternalGenerate(I: Integer): TAudioSample;
begin
  Result := Round(Level * sin( 2 * Pi * I * Frequency / SampleRate )
    * sin (2 * Pi * I * Envelope / SampleRate));
end;

end.
