unit CDI.Devices.AudioPlayer;

interface

uses
  System.Classes, System.SysUtils,
  WinApi.Windows, WinApi.MMSystem,
  CDI.Devices, CDI.AudioBuffer;

type
  TAudioPlayer = class;

  TAudioThread = class(TThread)
  private
    FAudioPlayer: TAudioPlayer;
    FLastError: integer;
  public
    constructor Create(AAudioPlayer: TAudioPlayer); reintroduce;
    procedure Execute; override;
    property LastError: integer read FLastError;
  end;

  TAudioPlayer = class(TCDIAudioDevice)
  private
    FAudioBuffer: TAudioBuffer;
    FAudioThread: TAudioThread;
    FCycling: boolean;
    procedure SetCycling(const Value: boolean);
  protected
    procedure InternalStart; override;
    procedure InternalPause; override;
    procedure InternalStop; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AudioBuffer: TAudioBuffer read FAudioBuffer write FAudioBuffer;
    property Cycling: boolean read FCycling write SetCycling;
  end;

const
  BUFFER_SIZE = 32768;

implementation

{ TAudioPlayer }

constructor TAudioPlayer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TAudioPlayer.Destroy;
begin
  inherited Destroy;
end;

procedure TAudioPlayer.InternalPause;
begin
  //
end;

procedure TAudioPlayer.InternalStart;
begin
  if FAudioBuffer = nil then
    Exit;

  FAudioThread := TAudioThread.Create(Self);
  FAudioThread.Start;
end;

procedure TAudioPlayer.InternalStop;
begin
  FAudioThread.Terminate;
  FAudioThread := nil;
end;

procedure TAudioPlayer.SetCycling(const Value: boolean);
begin
  FCycling := Value;
end;

{ TAudioThread }

constructor TAudioThread.Create(AAudioPlayer: TAudioPlayer);
begin
  inherited Create(True);
  FLastError := 0;
  FAudioPlayer := AAudioPlayer;
  FreeOnTerminate := True;
end;

procedure TAudioThread.Execute;
var
  WaveOut: THandle;
  WaveFormat: TWaveFormatex;
  WaveHeaders: array[0..1] of TWaveHdr;
  HEvent: THandle;
  HBuffer: THandle;
  Buffers: array[0..1] of Pointer;
  PAudio: Pointer;
  I: integer;
begin
  FillChar(WaveFormat, Sizeof(TWAVEFORMATEX), #0);
  with WaveFormat do
  begin
    wFormatTag      := WAVE_FORMAT_PCM;
    nChannels       := FAudioPlayer.Properties.Channels;
    nSamplesPerSec  := FAudioPlayer.Properties.SampleRate;
    wBitsPerSample  := FAudioPlayer.Properties.BitsPerSample;
    nBlockAlign     := FAudioPlayer.Properties.BlockAlign;
    nAvgBytesPerSec := FAudioPlayer.Properties.BytesRate;
    cbSize := 0;
  end;

  HEvent := CreateEvent(nil, False, False, nil);
  FLastError := WaveOutOpen(@WaveOut, 0, @WaveFormat, HEvent, 0, CALLBACK_EVENT);
  if FLastError <> 0 then
  begin
    CloseHandle(HEvent);
    Exit;
  end;

  // �������� ������ ��� ����� � ��������� �������, ��� ������� �����������
  Buffers[0] := VirtualAlloc(nil, BUFFER_SIZE * 2, MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
  Buffers[1] := Pointer(Longint(Buffers[0]) + BUFFER_SIZE);

  for I := 0 to 1 do
  begin
    FillChar(WaveHeaders[I], SizeOf(TWaveHdr), #0);
    WaveHeaders[I].lpData         := Buffers[I];
    WaveHeaders[I].dwBufferLength := BUFFER_SIZE;
    waveOutPrepareHeader(WaveOut, @WaveHeaders[I], SizeOf(TWaveHdr));
  end;

  // ���� ������ �� �����, � ��� ����� ������������� ������������ �������
  PAudio := FAudioPlayer.AudioBuffer.PBuffer;
  CopyMemory(Buffers[0], PAudio, BUFFER_SIZE);
  I := 0;
  while not Terminated do
  begin
    waveOutWrite(WaveOut, @WaveHeaders[I], SizeOf(TWaveHdr));
    WaitForSingleObject(HEvent, INFINITE);
    I := 1 - I;
    PAudio := Pointer(Longint(PAudio) + BUFFER_SIZE);
    CopyMemory(Buffers[I], PAudio, BUFFER_SIZE);
  end;

  waveOutReset(WaveOut);
  waveOutUnprepareHeader(WaveOut, @WaveHeaders[0], SizeOf(TWaveHdr));
  waveOutUnprepareHeader(WaveOut, @WaveHeaders[1], SizeOf(TWaveHdr));
  VirtualFree(Buffers[0], 0, MEM_RELEASE);
  WaveOutClose(WaveOut);
  CloseHandle(HEvent);
  FLastError := 0;
end;

end.
