unit CDI.Devices;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections, System.Types,
  Winapi.Windows, Winapi.Messages, Winapi.MMSystem;

type
  TCDIDevice = class(TComponent)
  end;

  TCDIMediaState = (msStopped, msActive, msPaused);

  TCDIMediaDevice = class(TCDIDevice)
  private
    FActive: boolean;
    procedure SetActive(const Value: boolean);
  protected
    FWindowHandle: THandle;
    procedure WndProc(var Message: TMessage); virtual;
    procedure InternalStart; virtual;
    procedure InternalStop; virtual;
    procedure InternalPause; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Start;
    procedure Stop;
    procedure Pause;
  published
    property Active: boolean read FActive write SetActive;
  end;

  TCDIAudioProperties = class(TPersistent)
  private
    class var FDefaultProperties: TCDIAudioProperties;
  private
    FOwner: TComponent;
    FSampleRate: Longint;
    FBitsPerSample: Word;
    FChannels: Word;
    function GetBlockAlign: Word;
    function GetBytesRate: Longint;
    class constructor Create;
    class destructor Destroy;
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(AOwner: TComponent);
    procedure Assign(Source: TPersistent); override;
    class function DefaultProperties: TCDIAudioProperties;
  published
    property SampleRate: Longint read FSampleRate;
    property BitsPerSample: Word read FBitsPerSample;
    property Channels: Word read FChannels;
    property BlockAlign: Word read GetBlockAlign;
    property BytesRate: Longint read GetBytesRate;
  end;

  TCDIAudioDevice = class(TCDIMediaDevice)
  private
    function GetProperties: TCDIAudioProperties;
  public
    property Properties: TCDIAudioProperties read GetProperties;
  end;

  TAudioSample = Smallint;
  TAudioSamples = array[0..0] of TAudioSample;
  PAudioSamples = ^TAudioSamples;

implementation

{ TAudioParams }

procedure TCDIAudioProperties.Assign(Source: TPersistent);
begin
  if Source is TCDIAudioProperties then
    with TCDIAudioProperties(Source) do
    begin
      Self.FSampleRate    := SampleRate;
      Self.FBitsPerSample := BitsPerSample;
      Self.FChannels      := Channels;
    end;
end;

constructor TCDIAudioProperties.Create(AOwner: TComponent);
begin
  FOwner := AOwner;
  FSampleRate := 44100;
  FBitsPerSample := 16;
  FChannels := 2;
end;

class constructor TCDIAudioProperties.Create;
begin
  FDefaultProperties := TCDIAudioProperties.Create(nil);
end;

class function TCDIAudioProperties.DefaultProperties: TCDIAudioProperties;
begin
  Result := FDefaultProperties;
end;

class destructor TCDIAudioProperties.Destroy;
begin
  FDefaultProperties.Free;
end;

function TCDIAudioProperties.GetBlockAlign: Word;
begin
  Result := FChannels * (FBitsPerSample div 8);
end;

function TCDIAudioProperties.GetBytesRate: LongInt;
begin
  Result := FSampleRate * BlockAlign;
end;

function TCDIAudioProperties.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

{ TCDIMediaDevice }

constructor TCDIMediaDevice.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWindowHandle := AllocateHWnd(WndProc);
end;

destructor TCDIMediaDevice.Destroy;
begin
  Stop;

  DeallocateHWnd(FWindowHandle);
  FWindowHandle := 0;

  inherited Destroy;
end;

procedure TCDIMediaDevice.InternalPause;
begin
end;

procedure TCDIMediaDevice.InternalStart;
begin
end;

procedure TCDIMediaDevice.InternalStop;
begin
end;

procedure TCDIMediaDevice.Pause;
begin
  InternalPause;
end;

procedure TCDIMediaDevice.SetActive(const Value: boolean);
begin
  if Active <> Value then
  begin
    if Value then
      Start
    else
      Stop;
  end;
end;

procedure TCDIMediaDevice.Start;
begin
  InternalStart;
  FActive := True;
end;

procedure TCDIMediaDevice.Stop;
begin
  FActive := False;
  InternalStop;
end;

procedure TCDIMediaDevice.WndProc(var Message: TMessage);
begin
  with Message do
    Result := DefWindowProc(FWindowHandle, Msg, wParam, lParam);
  Dispatch(Message);
end;

{ TCDIAudioDevice }

function TCDIAudioDevice.GetProperties: TCDIAudioProperties;
begin
  Result := TCDIAudioProperties.DefaultProperties;
end;

end.
