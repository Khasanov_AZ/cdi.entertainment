unit CDI.Devices.Microphone;

interface

uses
  System.Classes, System.SysUtils, System.Types, System.Generics.Collections,
  Winapi.Windows, Winapi.Messages, Winapi.MMSystem,
  CDI.Devices;

type
  TMicrophone = class;

  TMicrophoneNotifyType = (ntStart, ntStop, ntData);

  IMicrophoneListener = interface
    procedure MicrophoneNotify(AMicrophone: TMicrophone; ANotifyType: TMicrophoneNotifyType;
      ABuffer: PAudioSamples; ABufferSize: integer);
  end;

  TMicrophone = class(TCDIAudioDevice)
  private
    HBuffer: THandle;
    FWaveIn: HWAVEIN;
    FWaveHdr: TWaveHdr;
    FLevel: TAudioSample;
    FListeners: TList<IMicrophoneListener>;
    procedure MMWIMData(var Message: TMessage); message MM_WIM_DATA;
    function GetBufferSize: integer;
  protected
    procedure Notify(ANotifyType: TMicrophoneNotifyType; ABuffer: PAudioSamples = nil);
    procedure InternalStart; override;
    procedure InternalStop; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AddListener(AListener: IMicrophoneListener);
    procedure RemoveListener(AListener: IMicrophoneListener);
  published
    property BufferSize: integer read GetBufferSize;
    property Level: TAudioSample read FLevel;
  end;

implementation

const
  BUFFER_SIZE = 8192;

{ TMicrophone }

procedure TMicrophone.AddListener(AListener: IMicrophoneListener);
begin
  FListeners.Add(AListener);
end;

constructor TMicrophone.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FListeners := TList<IMicrophoneListener>.Create;
end;

destructor TMicrophone.Destroy;
begin
  FreeAndNil(FListeners);

  inherited Destroy;
end;

function TMicrophone.GetBufferSize: integer;
begin
  Result := BUFFER_SIZE;
end;

procedure TMicrophone.MMWIMData(var Message: TMessage);
var
  I: integer;
  AudioSamples: PAudioSamples;
  S: single;
begin
  if not Active then
    Exit;

  AudioSamples := PAudioSamples(PWaveHdr(Message.lParam)^.lpData);

  Notify(ntData, AudioSamples);

  S := 0;
  for I := 0 to BufferSize div SizeOf(TAudioSample) - 1 do
    S := S + Abs(AudioSamples^[I]);
  FLevel := Round(S * SizeOf(TAudioSample) / BufferSize);

  WaveInAddBuffer(FWaveIn, PWaveHdr(Message.lParam), SizeOf(TWaveHdr));
end;

procedure TMicrophone.Notify(ANotifyType: TMicrophoneNotifyType; ABuffer: PAudioSamples);
var
  Listener: IMicrophoneListener;
begin
  if FListeners <> nil then
    for Listener in FListeners do
      Listener.MicrophoneNotify(Self, ANotifyType, ABuffer, BufferSize);
end;

procedure TMicrophone.RemoveListener(AListener: IMicrophoneListener);
begin
  FListeners.Remove(AListener);
end;

procedure TMicrophone.InternalStart;
var
  WaveFormat: TWaveFormatEx;
  BufLen: NativeUInt;
  Buffer: Pointer;
  MMSysError: Cardinal;
begin
  WaveFormat.wFormatTag      := WAVE_FORMAT_PCM;
  WaveFormat.nChannels       := Properties.Channels;
  WaveFormat.nSamplesPerSec  := Properties.SampleRate;
  WaveFormat.wBitsPerSample  := Properties.BitsPerSample;
  WaveFormat.nBlockAlign     := Properties.BlockAlign;
  WaveFormat.nAvgBytesPerSec := Properties.BytesRate;
  WaveFormat.cbSize          := 0;

  MMSysError := waveInOpen(@FWaveIn, WAVE_MAPPER, @WaveFormat, FWindowHandle, 0, CALLBACK_WINDOW);
  if MMSysError <> 0 then
    raise Exception.CreateFmt('WaveInOpen returns error = %d', [MMSysError]);

  BufLen  := BufferSize;
  HBuffer := GlobalAlloc(GMEM_MOVEABLE and GMEM_SHARE, BufLen);
  Buffer  := GlobalLock(HBuffer);

  FWaveHdr.lpData         := Buffer;
  FWaveHdr.dwBufferLength := BufLen;
  FWaveHdr.dwFlags        := WHDR_BEGINLOOP;

  waveInPrepareHeader(FWaveIn, Addr(FWaveHdr), SizeOf(FWaveHdr));
  waveInAddBuffer(FWaveIn, Addr(FWaveHdr), SizeOf(FWaveHdr));

  WaveInStart(FWaveIn);
end;

procedure TMicrophone.InternalStop;
begin
  if not Active then
    Exit;

  FLevel := 0;

  WaveInReset(FWaveIn);
  WaveInUnPrepareHeader(FWaveIn, @FWaveHdr, SizeOf(FWaveHdr));
  WaveInClose(FWaveIn);
  GlobalUnlock(HBuffer);
  GlobalFree(HBuffer);
end;

end.
