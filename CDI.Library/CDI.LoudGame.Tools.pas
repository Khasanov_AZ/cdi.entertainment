unit CDI.LoudGame.Tools;

interface

uses
  System.Types, System.SysUtils, System.StrUtils, System.Classes, System.Generics.Collections,
  CDI.LoudGame;

type
  TLoudGameShapeRepository = class
  private
    class var FShapeClasses: TList<TLoudGameBlockClass>;
    class var FClassByName: TDictionary<string, TLoudGameShapeClass>;
    class var FNameByClass: TDictionary<TLoudGameShapeClass, string>;
  protected
    class constructor Create;
    class destructor Destroy;
  public
    class procedure RegisterShapeClass(AClass: TLoudGameShapeClass; AName: string);
    class function GetClass(AName: string): TLoudGameShapeClass;
    class function GetName(AClass: TLoudGameShapeClass): string;
  end;

  TLoudGameFileReadWriteService = class
  public
    class procedure SaveToFile(ACollection: TLoudGameBlocks; AFileName: string);
    class procedure LoadFromFile(ACollection: TLoudGameBlocks; AFileName: string);
  end;

implementation

{ TLoudGameFileReadWriteService }

class procedure TLoudGameFileReadWriteService.LoadFromFile(ACollection: TLoudGameBlocks; AFileName: string);
 procedure Parse(AText: string);
 var
   ShapeClass: TLoudGameShapeClass;
   Shape: TLoudGameBlock;
   Name, Params, P: string;
   Arr: TStringDynArray;
 begin
   Arr := SplitString(AText, ':');
   Name := Arr[0];
   Params := Arr[1];
   ShapeClass := TLoudGameShapeRepository.GetClass(Name);
   if ShapeClass = nil then
     Exit;

   Shape := ACollection.Add(ShapeClass);
   Arr := SplitString(Params, ';');
   Shape.Start  := StrToInt(Arr[0]);
   Shape.Length := StrToInt(Arr[1]);
   Shape.Level  := StrToInt(Arr[2]);
 end;
var
  F: TextFile;
  S: string;
begin
  AssignFile(F, AFileName);
  Reset(F);
  try
    while not Eof(F) do
    begin
      Readln(F, S);
      Parse(S);
    end;
  finally
    Close(F);
  end;
end;

class procedure TLoudGameFileReadWriteService.SaveToFile(
  ACollection: TLoudGameBlocks; AFileName: string);
  function ShapeToStr(AShape: TLoudGameBlock): string;
  var
    Name, Params: string;
  begin
    Name := TLoudGameShapeRepository.GetName(TLoudGameShapeClass(AShape.ClassType));
    Params := AShape.Start.ToString + ';' + AShape.Finish.ToString + ';' + AShape.Level.ToString;
    Result := Name + ':' + Params;
  end;
var
  F: TextFile;
  I: Integer;
begin
  AssignFile(F, AFileName);
  ReWrite(F);
  try
    for I := 0 to ACollection.Count - 1 do
      Writeln(F, ShapeToStr(ACollection[I]));
  finally
    Close(F);
  end;
end;

{ TLoudGameShapeRepository }

class constructor TLoudGameShapeRepository.Create;
begin
  FShapeClasses := TList<TLoudGameShapeClass>.Create;
  FClassByName  := TDictionary<string, TLoudGameShapeClass>.Create;
  FNameByClass  := TDictionary<TLoudGameShapeClass, string>.Create;
end;

class destructor TLoudGameShapeRepository.Destroy;
begin
  FShapeClasses.Free;
  FClassByName.Free;
  FNameByClass.Free;
end;

class function TLoudGameShapeRepository.GetClass(AName: string): TLoudGameShapeClass;
begin
  if FClassByName.ContainsKey(AName) then
    Result := FClassByName[AName]
  else
    Result := nil;
end;

class function TLoudGameShapeRepository.GetName(AClass: TLoudGameShapeClass): string;
begin
  if FNameByClass.ContainsKey(AClass) then
    Result := FNameByClass[AClass]
  else
    Result := '';
end;

class procedure TLoudGameShapeRepository.RegisterShapeClass(
  AClass: TLoudGameShapeClass; AName: string);
begin
  if not FShapeClasses.Contains(AClass) then
  begin
    FShapeClasses.Add(AClass);
    FClassByName.Add(AName, AClass);
    FNameByClass.Add(AClass, AName);
  end;
end;

end.
