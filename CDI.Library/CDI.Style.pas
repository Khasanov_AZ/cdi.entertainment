unit CDI.Style;

interface

uses
  System.Classes, System.SysUtils, System.Types, System.UITypes,
  VCL.Graphics;

type
  TCDIStyle = class
  public
    class function BackgroundColor: TColor;
    class function BorderColor: TColor;
  end;
  TCDIStyleClass = class of TCDIStyle;

var
  Style: TCDIStyleClass = TCDIStyle;

implementation

{ TCDIStyle }

class function TCDIStyle.BackgroundColor: TColor;
begin
  Result := clBlack;
end;

class function TCDIStyle.BorderColor: TColor;
begin
  Result := clWhite;
end;

end.
