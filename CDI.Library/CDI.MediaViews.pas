unit CDI.MediaViews;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections, System.Types,
  Winapi.Windows, Winapi.Messages, Winapi.MMSystem,
  Vcl.Controls, Vcl.Graphics, Vcl.Dialogs,
  CDI.Style,
  WaveIO, WaveStorage;

type
  TWaveAudioLevel = (walPeak, walAverage);

  TWaveAudioCustomView = class(TCustomControl, IWaveAudioIOListener)
  private
    FWaveAudio: TWaveAudioIO;
    FUseLevel: TWaveAudioLevel;
    procedure SetWaveAudio(const Value: TWaveAudioIO);
    procedure SetUseLevel(const Value: TWaveAudioLevel);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure WaveAudioLevel(Sender: TWaveAudioIO; APeakLevel, AAvgLevel: integer);
    procedure ProcessLevel(ALevel: integer); virtual;
  public
  published
    property WaveAudio: TWaveAudioIO read FWaveAudio write SetWaveAudio;
    property UseLevel: TWaveAudioLevel read FUseLevel write SetUseLevel default walPeak;
  end;

  TWaveAudioLevelView = class(TWaveAudioCustomView)
  private
    FLevel: integer;
    procedure SetLevel(const Value: integer);
  protected
    procedure Paint; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure ProcessLevel(ALevel: integer); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Align;
    property Level: integer read FLevel write SetLevel;
  end;

  TWaveAudioView = class(TWaveAudioCustomView)
  private
    FLevels: TList<Integer>;
    FDelimiters: TList<integer>;
    FWaveStorage: TWaveStorage;
    FPixelPerSecond: integer;
    procedure WMEraseBkgnd(var Msg: TMessage); message WM_ERASEBKGND;
    procedure SetWaveStorage(const Value: TWaveStorage);
    procedure SetPixelPerSecond(const Value: integer);
    function GetPixelLength: integer;
    function GetDelimiterCount: integer;
    function GetDelimiter(AIndex: integer): integer;
  protected
    procedure Paint; override;
    procedure AddLevel(ALevel: integer);
    procedure ProcessLevel(ALevel: integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function TimeAt(X: integer): integer;
    procedure AddDelimiter(ATime: integer);
    procedure RemoveDelimiter(AIndex: integer);
    procedure ClearDelimiters;
    function DelimiterAt(X: integer): integer;
    property DelimiterCount: integer read GetDelimiterCount;
    property Delimiters[AIndex: integer]: integer read GetDelimiter;
  published
    property Align;
    property WaveStorage: TWaveStorage read FWaveStorage write SetWaveStorage;
    property PixelLength: integer read GetPixelLength;
    property PixelPerSecond: integer read FPixelPerSecond write SetPixelPerSecond;
    property OnClick;
    property OnDblClick;
  end;

  TWaveView = class(TCustomControl)
  private
    FWave: TWaveStreamAdapter;
    FPixelPerSecond: integer;
    FMarkers: TList<integer>;
    procedure SetWave(const Value: TWaveStreamAdapter);
    procedure WaveData(Sender: TObject);
    procedure SetPixelPerSecond(const Value: integer);
    function GetMarkerCount: integer;
    function GetMarker(AIndex: integer): integer;
    function GetPixelLength: integer;
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AddMarker(AMarker: integer);
    procedure ClearMarkers;
    function TimeAt(X: integer): integer;
    function MarkerArray: TArray<integer>;
    property MarkerCount: integer read GetMarkerCount;
    property Markers[AIndex: integer]: integer read GetMarker;
    property PixelLength: integer read GetPixelLength;
    property Wave: TWaveStreamAdapter read FWave write SetWave;
  published
    property Align;
    property PixelPerSecond: integer read FPixelPerSecond write SetPixelPerSecond;
    property OnClick;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;

implementation

{ TWaveAudioCustomView }

procedure TWaveAudioCustomView.Notification(AComponent: TComponent; Operation: TOperation);
begin
  if (AComponent = WaveAudio) and (Operation = opRemove) then
    WaveAudio := nil;
  inherited Notification(AComponent, Operation);
end;

procedure TWaveAudioCustomView.ProcessLevel(ALevel: integer);
begin
end;

procedure TWaveAudioCustomView.SetUseLevel(const Value: TWaveAudioLevel);
begin
  FUseLevel := Value;
end;

procedure TWaveAudioCustomView.SetWaveAudio(const Value: TWaveAudioIO);
begin
  if WaveAudio <> Value then
  begin
    if FWaveAudio <> nil then
      FWaveAudio.RemoveListener(Self);
    FWaveAudio := Value;
    if FWaveAudio <> nil then
      FWaveAudio.AddListener(Self);
  end;
end;

procedure TWaveAudioCustomView.WaveAudioLevel(Sender: TWaveAudioIO; APeakLevel, AAvgLevel: integer);
begin
  if UseLevel = walPeak then
    ProcessLevel(APeakLevel)
  else
    Processlevel(AAvgLevel);
end;

{ TWaveAudioLevelView }

constructor TWaveAudioLevelView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width := 32;
  Height := 134;
end;

procedure TWaveAudioLevelView.Paint;
var
  R, Size, Cy, Cx: integer;
begin
  Canvas.Brush.Color := Style.BackgroundColor;
  Canvas.Pen.Color := Style.BorderColor;
  Canvas.Rectangle(ClientRect);

  R := Width div 2 - 2;
  Size := Height - 2 * R - 4;

  Cy := Height - MulDiv(Level, Size, 100) - R - 2;
  Cx := Width div 2;
  Canvas.Brush.Color := clYellow;
  Canvas.Ellipse(Cx - R, Cy - R, Cx + R, Cy + R);
end;

procedure TWaveAudioLevelView.ProcessLevel(ALevel: integer);
begin
  Level := ALevel;
end;

procedure TWaveAudioLevelView.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if AHeight < 48 then
    AHeight := 48;
  inherited SetBounds(ALeft, ATop, 32, AHeight);
end;

procedure TWaveAudioLevelView.SetLevel(const Value: integer);
begin
  FLevel := Value;
  Repaint;
end;

{ TWaveAudioView }

procedure TWaveAudioView.AddDelimiter(ATime: integer);
begin
  FDelimiters.Add(ATime);
  Repaint;
end;

procedure TWaveAudioView.AddLevel(ALevel: integer);
begin
  FLevels.Add(ALevel);
  Repaint;
end;

procedure TWaveAudioView.ClearDelimiters;
begin
  FDelimiters.Clear;
  Repaint;
end;

constructor TWaveAudioView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FLevels     := TList<Integer>.Create;
  FDelimiters := TList<Integer>.Create;
  FPixelPerSecond := 10;
  DoubleBuffered := True;
end;

function TWaveAudioView.DelimiterAt(X: integer): integer;
begin

end;

destructor TWaveAudioView.Destroy;
begin
  FLevels.Free;
  FDelimiters.Free;
  inherited Destroy;
end;

function TWaveAudioView.GetDelimiter(AIndex: integer): integer;
begin
  Result := FDelimiters[AIndex];
end;

function TWaveAudioView.GetDelimiterCount: integer;
begin
  Result := FDelimiters.Count;
end;

function TWaveAudioView.GetPixelLength: integer;
begin
  Result := 1000 div PixelPerSecond;
end;

procedure TWaveAudioView.Paint;
var
  R: TRect;
  L, I, J, F, Sum, SamplePerPixel, Lvl: integer;
  PS: PSmallint;
  PData: Pointer;
  Cont: boolean;
  S, S2: Smallint;
  WF: PWaveFormatEx;
begin
  R := ClientRect;

  Canvas.Brush.Color := Style.BackgroundColor;
  Canvas.Pen.Color   := Style.BorderColor;
  Canvas.Pen.Width   := 1;
  Canvas.Rectangle(ClientRect);
  Canvas.Font.Color := clWhite;

  L := Height div 2;
  Canvas.MoveTo(0, L);
  Canvas.LineTo(Width, L);

  I := 1;
  while I * PixelPerSecond < Width do
  begin
    Canvas.MoveTo(I * PixelPerSecond, 0);
    if I mod 10 = 0 then
      J := 15
    else if I mod 5 = 0 then
      J := 10
    else
      J := 5;
    Canvas.LineTo(I * PixelPerSecond, J);
    if I mod 10 = 0 then
      Canvas.TextOut(I * PixelPerSecond - Canvas.TextWidth(I.ToString) div 2, 15, I.ToString);
    inc(I);
  end;

  if (WaveStorage = nil) or (csDesigning in ComponentState) then
    Exit;

  SamplePerPixel := 88200 div PixelPerSecond;
//  SamplePerPixel := 4000;//(WaveStorage.Wave.WaveFormat.nSamplesPerSec * FPixelLength ) div 2000;
  F := 0; J := 0; Cont := True; Sum := 0;

  PData := TCustomMemoryStream(WaveStorage.Wave.Stream).Memory;
  Inc(PByte(PData), WaveStorage.Wave.DataOffset);
  PS := PData;
  for I := 0 to WaveStorage.Wave.Stream.Size div 2 - 1 do
  begin
    Sum := Sum + Abs(PS^);
    inc(J);
    if J > SamplePerPixel then
    begin
      Lvl := Round(L * (Sum / SamplePerPixel / $7FFF));
      Canvas.MoveTo(F, L - Lvl);
      Canvas.LineTo(F, L + Lvl);
      inc(F);
      J := 0;
      Sum := 0;
    end;
    inc(PS);
  end;

//  for I := 0 to FLevels.Count - 1 do
//  begin
//    Canvas.MoveTo(I, L - MulDiv(L, FLevels[I], 100));
//    Canvas.LineTo(I, L + MulDiv(L, FLevels[I], 100));
//  end;

//  if FLevels.Count > 0 then
//    Canvas.TextOut(10, 10, FLevels.Last.ToString);

  Canvas.Pen.Color := clYellow;
  Canvas.Pen.Width := 3;
  for I := 0 to FDelimiters.Count - 1 do
  begin
    F := (FDelimiters[I] div PixelLength);
    Canvas.MoveTo(F, 0);
    Canvas.LineTo(F, Height);
  end;

end;

procedure TWaveAudioView.ProcessLevel(ALevel: integer);
begin
  AddLevel(ALevel);
end;

procedure TWaveAudioView.RemoveDelimiter(AIndex: integer);
begin

end;

procedure TWaveAudioView.SetPixelPerSecond(const Value: integer);
begin
  FPixelPerSecond := Value;
  Repaint;
end;

procedure TWaveAudioView.SetWaveStorage(const Value: TWaveStorage);
begin
  FWaveStorage := Value;
end;

function TWaveAudioView.TimeAt(X: integer): integer;
begin
  if WaveAudio <> nil then
    Result := X * PixelLength;
end;

procedure TWaveAudioView.WMEraseBkgnd(var Msg: TMessage);
begin
  Msg.Result := -1;
end;

{ TWaveView }

procedure TWaveView.AddMarker(AMarker: integer);
begin
  FMarkers.Add(AMarker);
  Repaint;
end;

procedure TWaveView.ClearMarkers;
begin
  FMarkers.Clear;
  Repaint;
end;

constructor TWaveView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMarkers := TList<integer>.Create;
  FPixelPerSecond := 20;
  DoubleBuffered := True;
end;

destructor TWaveView.Destroy;
begin
  FMarkers.Free;
  inherited Destroy;
end;

function TWaveView.GetMarker(AIndex: integer): integer;
begin
  Result := FMarkers[AIndex];
end;

function TWaveView.GetMarkerCount: integer;
begin
  Result := FMarkers.Count;
end;

function TWaveView.GetPixelLength: integer;
begin
  Result := 1000 div PixelPerSecond;
end;

function TWaveView.MarkerArray: TArray<integer>;
begin
  Result := FMarkers.ToArray;
end;

procedure TWaveView.Paint;
var
  R: TRect;
  L, I, J, F, Sum, SamplePerPixel, Lvl: integer;
  PS: PSmallint;
  PData: Pointer;
  Cont: boolean;
  S, S2: Smallint;
  WF: PWaveFormatEx;
begin
  R := ClientRect;

  Canvas.Brush.Color := Style.BackgroundColor;
  Canvas.Pen.Color   := Style.BorderColor;
  Canvas.Pen.Width   := 1;
  Canvas.Rectangle(ClientRect);
  Canvas.Font.Color := clWhite;

  L := Height div 2;
  Canvas.MoveTo(0, L);
  Canvas.LineTo(Width, L);

  I := 1;
  while I * PixelPerSecond < Width do
  begin
    Canvas.MoveTo(I * PixelPerSecond, 0);
    if I mod 10 = 0 then
      J := 15
    else if I mod 5 = 0 then
      J := 10
    else
      J := 5;
    Canvas.LineTo(I * PixelPerSecond, J);
    if I mod 10 = 0 then
      Canvas.TextOut(I * PixelPerSecond - Canvas.TextWidth(I.ToString) div 2, 15, I.ToString);
    inc(I);
  end;

  if (Wave = nil) or (csDesigning in ComponentState) then
    Exit;

  SamplePerPixel := 88200 div PixelPerSecond;

  F := 0; J := 0; Cont := True; Sum := 0;

  PData := TCustomMemoryStream(Wave.Stream).Memory;
  Inc(PByte(PData), Wave.DataOffset);
  PS := PData;
  for I := 0 to Wave.Stream.Size div 2 - 1 do
  begin
    Sum := Sum + Abs(PS^);
    inc(J);
    if J > SamplePerPixel then
    begin
      Lvl := Round(L * (Sum / SamplePerPixel / $7FFF));
      Canvas.MoveTo(F, L - Lvl);
      Canvas.LineTo(F, L + Lvl);
      inc(F);
      J := 0;
      Sum := 0;
    end;
    inc(PS);
  end;

  Canvas.Pen.Color := clYellow;
  Canvas.Pen.Width := 3;
  for I := 0 to MarkerCount - 1 do
  begin
    F := (FMarkers[I] div PixelLength);
    Canvas.MoveTo(F, 0);
    Canvas.LineTo(F, Height);
  end;
end;

procedure TWaveView.SetPixelPerSecond(const Value: integer);
begin
  if (PixelPerSecond <> Value) and (Value > 1) then
  begin
    FPixelPerSecond := Value;
    Repaint;
  end;
end;

procedure TWaveView.SetWave(const Value: TWaveStreamAdapter);
begin
  if Wave <> Value then
  begin
    if Wave <> nil then
      Wave.OnData := nil;
    FWave := Value;
    if Wave <> nil then
      Wave.OnData := WaveData;
    Repaint;
  end;
end;

function TWaveView.TimeAt(X: integer): integer;
begin
  Result := X * PixelLength;
end;

procedure TWaveView.WaveData(Sender: TObject);
begin
  Repaint;
end;

end.
