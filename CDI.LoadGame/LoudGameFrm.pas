unit LoudGameFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CDI.LoudGame, Vcl.StdCtrls, mmSystem, Vcl.ExtCtrls,
  WaveUtils, WaveStorage, CDI.MediaViews, WaveIO, WaveIn, WaveRecorders, LoudGame.Karaoke, Vcl.MPlayer, Vcl.Buttons;

type
  TLoudGameForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    LiveAudioRecorder: TLiveAudioRecorder;
    TopPanel: TPanel;
    btnNewGame: TButton;
    btnReset: TButton;
    CaptionPanel: TPanel;
    CaptionLabel: TLabel;
    MainPanel: TPanel;
    btnStart: TButton;
    OpenDialog: TOpenDialog;
    WaveAudioLevelView: TWaveAudioLevelView;
    btnStop: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnNewGameClick(Sender: TObject);
    procedure LiveAudioRecorderLevel(Sender: TObject; Level: Integer);
    procedure btnResetClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnStopClick(Sender: TObject);
  private
    Competitors: TStringList;
    Karaoke: TCDIKaraoke;
    LoudGame: TLoudGame;
    GamePath: string;
    FState: TLoudGameState;
    procedure UpdateButtons;
    procedure OnGameStateChange(Sender: TLoudGame; AOldState, ANewState: TLoudGameState);
  public
    function AppPath: string;
  end;

var
  LoudGameForm: TLoudGameForm;

implementation

uses SelectDlg;

{$R *.dfm}

function TLoudGameForm.AppPath: string;
begin
  Result := ExtractFilePath(Application.ExeName);
end;

procedure TLoudGameForm.btnNewGameClick(Sender: TObject);
var
  Player, Game, MidiFileName, GameFileName: string;
begin
  if ShowSelectDialog(Competitors, GamePath, Player, Game) then
  begin
    CaptionLabel.Caption := Player + ' (' + Game + ')';

    MidiFileName := GamePath + Game + '.mid';
    GameFileName := GamePath + Game + '.game';
    if not FileExists(GameFileName) then
      GameFileName := GamePath + 'Default.game';

    LoudGame.LoadFromFile(MidiFileName, GameFileName);
  end;
end;

procedure TLoudGameForm.btnResetClick(Sender: TObject);
begin
  LoudGame.Reset;
end;

procedure TLoudGameForm.btnStartClick(Sender: TObject);
var
  FileName: string;
begin
  LiveAudioRecorder.Active :=  True;
  LoudGame.Start;
end;

procedure TLoudGameForm.btnStopClick(Sender: TObject);
begin
  LoudGame.Stop;
end;

procedure TLoudGameForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LiveAudioRecorder.Active := False;
end;

procedure TLoudGameForm.FormCreate(Sender: TObject);
begin
  Competitors := TStringList.Create;
  Competitors.LoadFromFile(AppPath + 'competitors.txt', TEncoding.UTF8);
  GamePath := AppPath + 'LoudGames\';

  LoudGame := TLoudGame.Create(Self);
  LoudGame.OnStateChange := OnGameStateChange;
  LoudGame.Parent := MainPanel;
  LoudGame.Align  := alClient;
  LoudGame.Length := 50000;
  LoudGame.Speed  := 500;
end;

procedure TLoudGameForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  Distance: integer;
begin
  Distance := 0;
  if Key = Ord('Q') then
    Distance := 5;
  if Key = Ord('A') then
    Distance  := -5;

  if Key = VK_SPACE then
    LoudGame.Start;

  if Distance <> 0 then
    LoudGame.Move(Distance);
end;

procedure TLoudGameForm.LiveAudioRecorderLevel(Sender: TObject; Level: Integer);
var
  L: integer;
begin
  L := Round(Level * Level * Level / 10000);
  LoudGame.Move( (L - LoudGame.BallLevel) / 10 );
end;

procedure TLoudGameForm.OnGameStateChange(Sender: TLoudGame; AOldState, ANewState: TLoudGameState);
begin
  FState := ANewState;
  UpdateButtons;
end;

procedure TLoudGameForm.UpdateButtons;
begin
  btnNewGame.Enabled := FState <> gsPlay;
  btnStart.Enabled := FState = gsReady;
  btnStop.Enabled  := FState = gsPlay;
end;

end.
