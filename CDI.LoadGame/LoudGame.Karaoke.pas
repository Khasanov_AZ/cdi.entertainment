unit LoudGame.Karaoke;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections;

type
  TCDIKaraokeBlock = class
  public
    Start: integer;
    Length: integer;
    Text: string;
    constructor Create(AStart, ALength: integer; AText: string);
  end;

  TCDIKaraoke = class
  private
    FFileName: string;
    FBlocks: TObjectList<TCDIKaraokeBlock>;
    FPulsePerQuarter: Double;
    FCurrentStart: integer;
    FFullText: string;
    FCurrentWord: string;
    FCurrentLength: integer;
    procedure ReadData;
    procedure Add(ALength: integer; AText: string);
    function GetBlock(AIndex: integer): TCDIKaraokeBlock;
    function GetCount: integer;
    function GetTextWithLengths: string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure LoadFromFile(AFileName: string);
    property FileName: string read FFileName;
    property FullText: string read FFullText;
    property TextWithLengths: string read GetTextWithLengths;
    property Count: integer read GetCount;
    property Blocks[AIndex: integer]: TCDIKaraokeBlock read GetBlock; default;
  end;

implementation

{ TCDIKaraoke }

procedure TCDIKaraoke.Add(ALength: integer; AText: string);
  procedure ProcessText(var ATxt: string);
  begin
    if Length(AText) > 0 then
    begin
      if ATxt[1] = '/' then
      begin
        FFullText := FFullText + #13#10;
        ATxt := ATxt.Substring(1);
      end
      else if ATxt[1] = '\' then
      begin
        FFullText := FFullText + #13#10#13#10;
        ATxt := ATxt.Substring(1);
      end;
    end;
  end;

  procedure PushText(ATxt: string);
  begin
    if ATxt <> '' then
      ProcessText(ATxt);
    FBlocks.Add(TCDIKaraokeBlock.Create(FCurrentStart, FCurrentLength, ATxt));
    FCurrentStart := FCurrentStart + FCurrentLength;
    FCurrentLength := 0;
    FCurrentWord := '';
  end;
begin
  if (AText.StartsWith('\') or AText.StartsWith('/')) and (FCurrentWord <> '') then
    PushText(FCurrentWord);

  ALength := Round(ALength / FPulsePerQuarter);
  FCurrentLength := FCurrentLength + ALength;
  FCurrentWord := FCurrentWord + AText;

  if Length(AText) = 0 then
    PushText('')
  else
    if (AText[AText.Length] = ' ') or (AText[AText.Length] = '\') or (AText[AText.Length] = '.') then
      PushText(FCurrentWord);

  ProcessText(AText);
  FFullText := FFullText + AText;
end;

procedure TCDIKaraoke.Clear;
begin
  FFileName := '';
  FCurrentStart := 0;
  FFullText := '';
  FBlocks.Clear;
end;

constructor TCDIKaraoke.Create;
begin
  FBlocks := TObjectList<TCDIKaraokeBlock>.Create(True);
end;

destructor TCDIKaraoke.Destroy;
begin
  FBlocks.Free;
  inherited Destroy;
end;

function TCDIKaraoke.GetBlock(AIndex: integer): TCDIKaraokeBlock;
begin
  Result := FBlocks[AIndex];
end;

function TCDIKaraoke.GetCount: integer;
begin
  Result := FBlocks.Count;
end;

function TCDIKaraoke.GetTextWithLengths: string;
var
  I: integer;
begin
  Result := '';
  for I := 0 to FBlocks.Count - 1 do
    Result := Result + FBlocks[I].Start.ToString + ' ' + FBlocks[I].LEngth.ToString + ' ' + FBlocks[I].Text + #13#10;
end;

procedure TCDIKaraoke.LoadFromFile(AFileName: string);
begin
  Clear;
  FFileName := AFileName;
  ReadData;
end;

procedure TCDIKaraoke.ReadData;
var
  Stream: TStringStream;
  Pos, I: Integer;
  B0, B1, B2, B3, B4, Chars: Byte;
  Found: boolean;
  L: Word;
  T: string;

  procedure ReadMHdr;
  begin
    FPulsePerQuarter := Stream.Bytes[12] * 256 + Stream.Bytes[13];  //40 41 42
  end;

  procedure ReadMHdr2;
  begin
    FPulsePerQuarter := (Stream.Bytes[Pos + 3] * $100 + Stream.Bytes[Pos + 4]) * $100 + Stream.Bytes[Pos + 5];
    FPulsePerQuarter := ((60000000 / FPulsePerQuarter) / 1000) * 2;
  end;

  function ReadByte: Byte;
  begin
    Result := Stream.Bytes[Pos];
    inc(Pos);
  end;

  function ReadLength: Word;
  var
    B, B2: byte;
  begin
    B := ReadByte;
    if B and $80 = 0 then
      Result := B
    else begin
      B2 := ReadByte;
      if B and $01 = 1 then
        B2 := B2 + $80;
      Result := $100 * ((B and $7F) div 2) + B2;
    end;
  end;

begin
  Stream := TStringStream.Create();
  try
    Stream.LoadFromFile(FileName);
    ReadMHdr;
    Pos := 14;
    // ���� @LRUS
    Found := False;
    while Pos < Stream.Size - 5 do
    begin
      B0 := Stream.Bytes[Pos + 0];
      B1 := Stream.Bytes[Pos + 1];
      B2 := Stream.Bytes[Pos + 2];
      B3 := Stream.Bytes[Pos + 3];
      B4 := Stream.Bytes[Pos + 4];
      if (B0 = $FF) and (B1 = $51) and (B2 = $3) then
        ReadMHdr2;

      if (B0 = $40) and (B1 = $4C) and (B2 = $52) and (B3 = $55) and (B4 = $53) then
      begin
        Found := True;
        Pos := Pos + 5;
        Break;
      end;
      inc(Pos);
    end;

    if not Found then
      raise Exception.Create('���� �� �������������� :(');

    L := ReadLength;
    Add(L, '');

    while True do
    begin
      B0 := ReadByte;
      B1 := ReadByte;
      if B0 <> $FF then
        raise Exception.Create('NOT $FF');
      if B1 = $2F then
        Break;
      if B1 <> $01 then
        raise Exception.Create('NOT $01');

      Chars := ReadByte;
      T := '';
      for I := 1 to Chars do
        T := T + AnsiChar(ReadByte);
      L := ReadLength;
      Add(L, T);
    end

  finally
    Stream.Free;
  end;
end;

{ TCDIKaraokeBlock }

constructor TCDIKaraokeBlock.Create(AStart, ALength: integer; AText: string);
begin
  Start := AStart;
  Length := ALength;
  Text := AText;
end;

end.
