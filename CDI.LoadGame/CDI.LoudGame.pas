﻿unit CDI.LoudGame;

interface

uses
  System.Classes, System.SysUtils, System.Generics.Collections,
  Winapi.Windows, Winapi.Messages,
  Vcl.Forms, Vcl.Controls, Vcl.Graphics, Vcl.ExtCtrls, Vcl.Imaging.Pngimage, Vcl.MPlayer,

  WaveIO, WaveTimer, CDI.Style, LoudGame.Karaoke;

type
  TLoudGame = class;
  TLoudGameBlocks = class;
  TLoudGameBlockClass = class of TLoudGameBlock;

  TPoints = TArray<TPoint>;

  TLoudGameState = (gsNone, gsReady, gsPlay, gsPaused, gsFailed, gsSuccess);

  TLoudBallState = (bsNormal, bsShouted, bsBreaked, bsPleased);
  TLoudBallSize = (lbsSmall = 24, lbsNormal = 32, lbsLarge = 48, lbsHuge = 64);
  TLoudGameStateChangeEvent = procedure(Sender: TLoudGame; AOldState, ANewState: TLoudGameState) of object;

  TLoudGameElement = class
  private
    FGame: TLoudGame;
  public
    constructor Create(AGame: TLoudGame); virtual;
    property Game: TLoudGame read FGame;
  end;

  TLoudBall = class(TLoudGameElement)
  private
    FName: string;
    FBallSize: integer;
    FImages: array[TLoudBallState] of TPNGImage;
    procedure CreateImages;
    function GetRadius: integer;
  public
    constructor Create(AGame: TLoudGame; ABallSize: TLoudBallSize; AName: string); reintroduce;
    destructor Destroy; override;
    property Radius: integer read GetRadius;
    property BallSize: integer read FBallSize;
  end;

  TLoudGameBlock = class(TLoudGameElement)
  private
    function GetFinish: integer;
  protected
    FStart: integer;
    FLength: integer;
    procedure SetData(AData: string); virtual;
    procedure Paint(ACanvas: TCanvas); virtual; abstract;
    function ToGame(APoint: TPoint): TPoint;
    function InBlock(X, Y: integer): boolean; virtual;
  public
    constructor Create(AGame: TLoudGame; AData: string); virtual;
    class function Name: string; virtual;
    function IsCollision(P: TPoint; R: integer): boolean;
    property Start: integer read FStart;
    property Length: integer read FLength;
    property Finish: integer read GetFinish;
  end;

  TLoudGamePolygonBlock = class(TLoudGameBlock)
  private
    FRGN: HRGN;
  protected
    FPolygon: TPoints;
    procedure Add(X, Y: integer);
    procedure Paint(ACanvas: TCanvas); override;
  public
    constructor Create(AGame: TLoudGame; AData: string); override;
    destructor Destroy; override;
  end;

  TLoudGameUpTrapBlock = class(TLoudGamePolygonBlock)
  private
    FLevel, FFront, FLen, FRear: integer;
  protected
    procedure SetData(AData: string); override; //Start;Level;Front;Length;Rear
    function InBlock(X, Y: integer): boolean; override;
  public
    class function Name: string; override;
  end;

  TLoudGameDownTrapBlock = class(TLoudGamePolygonBlock)
  private
    FLevel, FFront, FLen, FRear: integer;
  protected
    procedure SetData(AData: string); override; //Start;Level;Front;Length;Rear
    function InBlock(X, Y: integer): boolean; override;
  public
    class function Name: string; override;
  end;

  TLoudGameRectBlock = class(TLoudGamePolygonBlock)
  private
    FLevelUp, FLevelDown: integer;
  protected
    procedure SetData(AData: string); override; //Start;LevelDown;LevelUp;Length
    function InBlock(X, Y: integer): boolean; override;
  public
    class function Name: string; override;
  end;

  TLoudGameCircleBlock = class(TLoudGameBlock)
  private
    FRadius: integer;
    FCenter: TPoint;
  protected
    procedure SetData(AData: string); override; //Start;Level;Radius
    procedure Paint(ACanvas: TCanvas); override;
    function InBlock(X, Y: integer): boolean; override;
  public
    class function Name: string; override;
  end;

  TLoudGameDiamondBlock = class(TLoudGamePolygonBlock)
  protected
    procedure SetData(AData: string); override; //Start;Level;Radius
  public
    class function Name: string; override;
  end;

  TLoudGameBlocks = class
  private
    FGame: TLoudGame;
    FBlocks: TObjectList<TLoudGameBlock>;
    function GetBlock(AIndex: integer): TLoudGameBlock;
  public
    constructor Create(AGame: TLoudGame);
    destructor Destroy; override;

    function Add(ABlockClass: TLoudGameBlockClass; ABlockData: string): TLoudGameBlock; overload;
    function Count: integer;
    procedure Clear;
    procedure Delete(AIndex: integer);
    procedure Remove(ABlock: TLoudGameBlock);

    procedure LoadFromFile(const AFileName: string);

    property Blocks[AIndex: integer]: TLoudGameBlock read GetBlock; default;
  end;

  TTimerThread = class(TThread)
  private
    const TIMER_INTERVAL = 20;
  private
    FGame: TLoudGame;
    FTimer: TMultimediaTimer;
    procedure OnTimer(Sender: TObject);
    function GetInterval: Word;
  protected
    procedure Execute; override;
  public
    constructor Create(AGame: TLoudGame); reintroduce;
    destructor Destroy; override;

    property Interval: Word read GetInterval;
  end;

  TLoudGame = class(TCustomControl)
  private
    FBlocks: TLoudGameBlocks;
    FLength: integer;
    FSpeed: integer;
    FBitmap: TBitmap;
    FActive: boolean;
    FCurrentTime: integer;
    FCurrentLength: integer;
    FState: TLoudGameState;
    FBall: TLoudBall;
    FBallSize: TLoudBallSize;
    FBallLevel: single;
    FBallOffset: integer;
    FMediaPlayer: TMediaPlayer;
    FKaraoke: TCDIKaraoke;
    FTimerThread: TTimerThread;
    FOnStateChange: TLoudGameStateChangeEvent;
    procedure SetLength(const Value: integer);
    procedure SetSpeed(const Value: integer);
    procedure SetActive(const Value: boolean);
    procedure SetState(const Value: TLoudGameState);
    function Step: integer;
    procedure SetBallSize(const Value: TLoudBallSize); protected
    procedure PrepareBuffer;
    procedure RecreateBall;
    function GetGameRect: TRect;
    function GetOffset: integer;
  protected
    procedure OnTimer(Sender: TObject);
    procedure Paint; override;
    procedure Resize; override;

    property State: TLoudGameState read FState write SetState;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure LoadFromFile(const AMidiFileName, AGameFileName: string);

    procedure Reset;
    procedure Start;
    procedure Stop;
    procedure Repaint; override;
    property CurrentTime: integer read FCurrentTime;
    property CurrentLength: integer read FCurrentLength;
    procedure Move(ADistance: single);
    property BallLevel: single read FBallLevel;
    property Karaoke: TCDIKaraoke read FKaraoke;
  published
    property Align;
    property Active: boolean read FActive write SetActive;
    property Length: integer read FLength write SetLength;  // Длина в ПИКСЕЛАХ
    property Speed: integer read FSpeed write SetSpeed;     // Скорость в пикселах в секунду.
    property BallSize: TLoudBallSize read FBallSize write SetBallSize;
    property OnStateChange: TLoudGameStateChangeEvent read FOnStateChange write FOnStateChange;
  end;

  function GetBlockClass(AName: string): TLoudGameBlockClass;

implementation

var
  FBlockClasses: TList<TLoudGameBlockClass> = nil;

function GetBlockClass(AName: string): TLoudGameBlockClass;
begin
  for Result in FBlockClasses do
    if Result.Name = AName then
      Exit;
  Result := nil;
end;

procedure Parse2(AData: string; var AVal1, AVal2: integer);
var
  Arr: TArray<String>;
begin
  Arr := AData.Split([';']);
  AVal1 := Arr[0].ToInteger;
  AVal2 := Arr[1].ToInteger;
end;

procedure Parse3(AData: string; var AVal1, AVal2, AVal3: integer);
var
  Arr: TArray<String>;
begin
  Arr := AData.Split([';']);
  AVal1 := Arr[0].ToInteger;
  AVal2 := Arr[1].ToInteger;
  AVal3 := Arr[2].ToInteger;
end;

procedure Parse4(AData: string; var AVal1, AVal2, AVal3, AVal4: integer);
var
  Arr: TArray<String>;
begin
  Arr := AData.Split([';']);
  AVal1 := Arr[0].ToInteger;
  AVal2 := Arr[1].ToInteger;
  AVal3 := Arr[2].ToInteger;
  AVal4 := Arr[3].ToInteger;
end;

procedure Parse5(AData: string; var AVal1, AVal2, AVal3, AVal4, AVal5: integer);
var
  Arr: TArray<String>;
begin
  Arr := AData.Split([';']);
  AVal1 := Arr[0].ToInteger;
  AVal2 := Arr[1].ToInteger;
  AVal3 := Arr[2].ToInteger;
  AVal4 := Arr[3].ToInteger;
  AVal5 := Arr[4].ToInteger;
end;

{ TLoudGame }

constructor TLoudGame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FBitmap := TBitmap.Create;

  FBallSize := lbsHuge;
  DoubleBuffered := True;
  RecreateBall;

  FBlocks := TLoudGameBlocks.Create(Self);

  FLength := 30;
  FSpeed  := 100;
  FBallLevel := 50;
  FBallOffset := 25;

  FMediaPlayer := TMediaPlayer.Create(Self);
  FMediaPlayer.Visible := False;
  FMediaPlayer.Parent := Self;

  FKaraoke := TCDIKaraoke.Create;

  FTimerThread := TTimerThread.Create(Self);
  FTimerThread.Start;
  State := gsNone;
end;

destructor TLoudGame.Destroy;
begin
  Stop;

  FTimerThread.Free;
  FBitmap.Free;
  FMediaPlayer.Free;
  FKaraoke.Free;
  FreeAndNil(FBlocks);
  inherited Destroy;
end;

function TLoudGame.GetGameRect: TRect;
begin
  Result := ClientRect;
  Result.Top    := Result.Top;
  Result.Bottom := Result.Bottom - 80;
end;

function TLoudGame.GetOffset: integer;
begin
  Result := Muldiv(Width, FBallOffset, 100);
end;

procedure TLoudGame.LoadFromFile(const AMidiFileName, AGameFileName: string);
begin
  Reset;

  FKaraoke.LoadFromFile(AMidiFileName);
  FBlocks.LoadFromFile(AGameFileName);

  FMediaPlayer.FileName := AMidiFileName;
  FMediaPlayer.Open;
  State := gsReady;
end;

procedure TLoudGame.Move(ADistance: single);
begin
  if State = gsPlay then
  begin
    FBallLevel := FBallLevel + ADistance;
    if FBallLevel > 100 then
      FBallLevel := 100;

    if FBallLevel < 0 then
      FBallLevel := 0;
  end;
end;

procedure TLoudGame.OnTimer(Sender: TObject);
var
  I: integer;
  Failed: boolean;
  BallPos: TPoint;
  DC: HDC;
begin
  if State = gsPlay then
  begin
    FCurrentTime   := FCurrentTime    + FTimerThread.Interval;
    FCurrentLength := FCurrentLength  + Step;
  end;

  BallPos := Point(FCurrentLength, Round(FBallLevel));

  if State = gsPlay then
  begin
    Failed := False;
    for I := 0 to FBlocks.Count - 1 do
    begin
      Failed := FBlocks[I].IsCollision(BallPos, FBall.Radius);
      if Failed then
      begin
        State := gsFailed;
        Break;
      end;
    end;
  end;

  if State = gsPlay then
  begin
    if CurrentLength > Length then
      State := gsSuccess;
  end;

  PrepareBuffer;
  Invalidate;
end;

procedure TLoudGame.Paint;
begin
  Canvas.Draw(0, 0, FBitmap);
end;

procedure TLoudGame.PrepareBuffer;
  procedure DrawBall;
  var
    R, Size: integer;
    BallPos: TPoint;
    BallState: TloudBallState;
  begin
    R := FBall.Radius;
    Size := GetGameRect.Height - 2 * R - 4;

    BallPos.X := GetOffset - R;
    BallPos.Y := Round(Size * (1 - FBallLevel / 100) + 2);
    if FBallLevel > 80 then
      BallState := bsShouted
    else
      BallState := bsNormal;
    if State = gsFailed then
      BallState := bsBreaked;
    if State = gsSuccess then
      BallState := bsPleased;

    FBitmap.Canvas.Draw(BallPos.X, BallPos.Y, FBall.FImages[BallState]);
  end;

var
  I, F: integer;
  GR: TRect;
  var Start: integer;
  var StartPixel: integer;
begin
  GR := GetGameRect;

  FBitmap.Canvas.Brush.Style := bsSolid;
  FBitmap.Canvas.Brush.Color := TCDIStyle.BackgroundColor;
  FBitmap.Canvas.Pen.Color   := TCDIStyle.BorderColor;
  FBitmap.Canvas.FillRect(ClientRect);

  FBitmap.Canvas.Brush.Color := $101010;
  FBitmap.Canvas.FillRect(GR);

  FBitmap.Canvas.Pen.Width := 3;
  FBitmap.Canvas.MoveTo(GR.Left,  GR.Top);
  FBitmap.Canvas.LineTo(GR.Right, GR.Top);
  FBitmap.Canvas.MoveTo(GR.Left,  GR.Bottom);
  FBitmap.Canvas.LineTo(GR.Right, GR.Bottom);

  if State = gsNone then
    Exit;

  for I := 0 to FBlocks.Count - 1 do
    FBlocks[I].Paint(FBitmap.Canvas);

  FBitmap.Canvas.Font.Color := clWhite;
  FBitmap.Canvas.Font.Size := 12;
  FBitmap.Canvas.Brush.Color := clBlack;
  FBitmap.Canvas.TextOut(10, 10, 'Time: ' + FCurrentTime.ToString);
  FBitmap.Canvas.TextOut(10, 40, 'Pos: '  + FCurrentLength.ToString);
  // Финишная прямая
  if FCurrentLength > Length - Width - GetOffset then
  begin
    FBitmap.Canvas.Brush.Color := clWhite;
    FBitmap.Canvas.Brush.Style := bsSolid;
    F := Length - FCurrentLength + GetOffset;
    FBitmap.Canvas.FillRect(Rect(F - 20, GR.Top, F + 20, GR.Bottom));
  end;
  DrawBall;

  //DRAW KARAOKE TEXT
  Start := 0;
  FBitmap.Canvas.Font.Size := 48;
  FBitmap.Canvas.Brush.Style := bsClear;
  for I := 0 to FKaraoke.Count - 1 do
  begin
    Start := FKaraoke[I].Start - FCurrentTime + 1000;
    if Start > 0 then
    begin
      StartPixel := (Start * Speed) div 1000;
      FBitmap.Canvas.TextOut( StartPixel, Height - 80, AnsiUpperCase(FKaraoke[I].Text) );
    end;
  end;
end;

procedure TLoudGame.Reset;
begin
  FCurrentTime := 0;
  FCurrentLength := 0;
  FBallLevel := 50;
  if Active then
  begin
    FMediaPlayer.Stop;
    FMediaPlayer.Position := 0;
  end;
  FActive := False;
  State := gsReady;
end;

procedure TLoudGame.Resize;
begin
  inherited;
  FBitmap.Width  := Width;
  FBitmap.Height := Height;
end;

procedure TLoudGame.RecreateBall;
begin
  FreeAndNil(FBall);
  FBall := TLoudBall.Create(Self, FBallSize, 'SMILE');
end;

procedure TLoudGame.Repaint;
begin
  PrepareBuffer;
  inherited Repaint;
end;

procedure TLoudGame.SetActive(const Value: boolean);
begin
  if Active <> Value then
  begin
    if Value then
      Start
    else
      Stop;
  end;
end;

procedure TLoudGame.SetBallSize(const Value: TLoudBallSize);
begin
  if State <> gsReady then
    Exit;

  if BallSize <> Value then
  begin
    FBallSize := Value;
    RecreateBall;
  end;
end;

procedure TLoudGame.SetLength(const Value: integer);
begin
  FLength := Value;
end;

procedure TLoudGame.SetSpeed(const Value: integer);
begin
  FSpeed := Value;
end;

procedure TLoudGame.SetState(const Value: TLoudGameState);
var
  OldState: TLoudGameState;
begin
  if State <> Value then
  begin
    OldState := FState;
    FState := Value;
    if FState <> gsPlay then
      Stop;

    if Assigned(FOnStateChange) then
      FOnStateChange(Self, OldState, FState);
  end;
end;

procedure TLoudGame.Start;
begin
  if Active then
    Exit;

  FActive := True;
  FMediaPlayer.Play;
  State := gsPlay;
end;

function TLoudGame.Step: integer;
begin
  Result := Speed * FTimerThread.Interval div 1000;
end;

procedure TLoudGame.Stop;
begin
  if not Active then
    Exit;

  if not (csDestroying in ComponentState) then
    FMediaPlayer.Stop;
  FActive := False;
  State := gsReady;
end;

{ TLoudGameBlocks }

function TLoudGameBlocks.Add(ABlockClass: TLoudGameBlockClass; ABlockData: string): TLoudGameBlock;
begin
  Result := ABlockClass.Create(FGame, ABlockData);
  FBlocks.Add(Result);
end;

procedure TLoudGameBlocks.Clear;
begin
  FBlocks.Clear;
end;

function TLoudGameBlocks.Count: integer;
begin
  Result := FBlocks.Count;
end;

constructor TLoudGameBlocks.Create(AGame: TLoudGame);
begin
  inherited Create;
  FGame := AGame;
  FBlocks := TObjectList<TLoudGameBlock>.Create(True);
end;

procedure TLoudGameBlocks.Delete(AIndex: integer);
begin
  FBlocks.Delete(AIndex);
end;

destructor TLoudGameBlocks.Destroy;
begin
  FBlocks.Free;
  inherited Destroy;
end;

function TLoudGameBlocks.GetBlock(AIndex: integer): TLoudGameBlock;
begin
  Result := FBlocks[AIndex];
end;

procedure TLoudGameBlocks.LoadFromFile(const AFileName: string);
var
  I: integer;
  SL: TStringList;
  Str: TArray<string>;
begin
  SL := TStringList.Create;
  try
    SL.LoadFromFile(AFileName);
    Str := SL[0].Split([':']);
    FGame.Length := StrToInt(Str[1]);
    for I := 1 to SL.Count - 1 do
    begin
      Str := SL[I].Split([':']);
      Add(GetBlockClass(Str[0]), Str[1]);
    end;
  finally
    SL.Free;
  end;
end;

procedure TLoudGameBlocks.Remove(ABlock: TLoudGameBlock);
begin
  FBlocks.Remove(ABlock);
end;

{ TLoudBall }

constructor TLoudBall.Create(AGame: TLoudGame; ABallSize: TLoudBallSize; AName: string);
begin
  inherited Create(AGame);

  FBallSize := Integer(ABallSize);
  FName := AName;
  CreateImages;
end;

procedure TLoudBall.CreateImages;
  function CreateImage(AState: string): TPngImage;
  var
    FileName: string;
  begin
    Result := TPNGImage.Create;
    FileName := ExtractFilePath(Application.ExeName) + 'Resources\'+ FName + '_' + AState + '_' + FBallSize.ToString + '.png';
//    if not FileExists(FileName) then
//      FileName := ExtractFilePath(Application.ExeName) + 'Resources\'+ FName + '_NORMAL_' + FBallSize.ToString + '.png';

    Result.LoadFromFile(FileName);
//    Result.LoadFromResourceName(HInstance, FName + '_' + AState + '_' + FBallSize.ToString);
  end;
begin
  FImages[bsNormal]  := CreateImage('NORMAL');
  FImages[bsShouted] := CreateImage('SHOUTED');
  FImages[bsBreaked] := CreateImage('BREAKED');
  FImages[bsPleased] := CreateImage('PLEASED');
end;

destructor TLoudBall.Destroy;
begin
  FImages[bsNormal].Free;
  FImages[bsShouted].Free;
  FImages[bsBreaked].Free;
  FImages[bsPleased].Free;
  inherited Destroy;
end;

function TLoudBall.GetRadius: integer;
begin
  Result := FBallSize div 2;
end;

{ TLoudGamePolygonBlock }

procedure TLoudGamePolygonBlock.Add(X, Y: integer);
begin
  FPolygon := FPolygon + [Point(X, Y)];
end;

constructor TLoudGamePolygonBlock.Create(AGame: TLoudGame; AData: string);
begin
  inherited Create(AGame, AData);

  FRGN := CreatePolygonRgn(FPolygon, System.Length(FPolygon), WINDING);
end;

destructor TLoudGamePolygonBlock.Destroy;
begin
  DeleteObject(FRGN);
  inherited Destroy;
end;

procedure TLoudGamePolygonBlock.Paint(ACanvas: TCanvas);
var
  P: TPoint;
  Pl: TPoints;
begin
  Pl := [];
  for P in FPolygon do
    Pl := Pl + [ToGame(P)];

  ACanvas.Brush.Color := clWebBrown;
  ACanvas.Brush.Style := bsDiagCross;
  ACanvas.Pen.Color := clWhite;
  ACanvas.Pen.Width := 3;
  ACanvas.Polygon(Pl);
  ACanvas.Polyline(Pl);
end;

{ TLoudGameBlock }

constructor TLoudGameBlock.Create(AGame: TLoudGame; AData: string);
begin
  inherited Create(AGame);
  SetData(AData);
end;

function TLoudGameBlock.GetFinish: integer;
begin
  Result := FStart + FLength;
end;

function TLoudGameBlock.InBlock(X, Y: integer): boolean;
begin
  Result := False;
end;

function TLoudGameBlock.IsCollision(P: TPoint; R: integer): boolean;
begin
  Result := InBlock(P.X + R - 4, P.Y) or InBlock(P.X - R + 4, P.Y);
//    or InBlock(P.X, P.Y + R - 4) or InBlock(P.X, P.Y - R + 4);
end;

class function TLoudGameBlock.Name: string;
begin
  Result := '';
end;

procedure TLoudGameBlock.SetData(AData: string);
begin
end;

function TLoudGameBlock.ToGame(APoint: TPoint): TPoint;
begin
  Result.Y := Round(Game.GetGameRect.Height * (1 - APoint.Y / 100));
  Result.X := Game.GetOffset + (APoint.X - Game.CurrentLength);
end;

{ TLoudGameUpTrapBlock }

function TLoudGameUpTrapBlock.InBlock(X, Y: integer): boolean;
var
  L, R: integer;
begin
  L := Round(Start + FFront * (100 - Y) / FLevel);
  R := Round(Finish - FRear * (100 - Y) / FLevel);
  Result := (X > L) and (X < R) and (Y > (100 - FLevel));
end;

class function TLoudGameUpTrapBlock.Name: string;
begin
  Result := 'UPTRAP';
end;

procedure TLoudGameUpTrapBlock.SetData(AData: string);
begin
  Parse5(AData, FStart, FLevel, FFront, FLen, FRear);
  FLength := FFront + FLen + FRear;

  Add(FStart, 100);
  Add(FStart + FFront, 100 - FLevel);
  Add(FStart + FFront + FLen, 100 - FLevel);
  Add(FStart + FLength, 100);
end;

{ TLoudGameDownTrapBlock }

function TLoudGameDownTrapBlock.InBlock(X, Y: integer): boolean;
var
  L, R: integer;
begin
  L := Round(Start + FFront * Y / FLevel);
  R := Round(Finish - FRear * Y / FLevel);
  Result := (X > L) and (X < R) and (Y < FLevel);
end;

class function TLoudGameDownTrapBlock.Name: string;
begin
  Result := 'DOWNTRAP';
end;

procedure TLoudGameDownTrapBlock.SetData(AData: string);
begin
  Parse5(AData, FStart, FLevel, FFront, FLen, FRear);
  FLength := FFront + FLen + FRear;

  Add(FStart, 0);
  Add(FStart + FFront, FLevel);
  Add(FStart + FFront + FLen, FLevel);
  Add(FStart + FLength, 0);
end;

{ TLoudGameRectBlock }

function TLoudGameRectBlock.InBlock(X, Y: integer): boolean;
begin
  Result := (X >= Start) and (X <= Finish) and (Y >= FLevelDown) and (Y <= FLevelUp);
end;

class function TLoudGameRectBlock.Name: string;
begin
  Name := 'RECT';
end;

procedure TLoudGameRectBlock.SetData(AData: string);
begin
  Parse4(AData, FStart, FLevelDown, FLevelUp, FLength);

  Add(FStart, FLevelDown);
  Add(FStart, FLevelUp);
  Add(FStart + FLength, FLevelUp);
  Add(FStart + FLength, FLevelDown);
end;

{ TLoudGameCircleBlock }

function TLoudGameCircleBlock.InBlock(X, Y: integer): boolean;
begin
  Result := FCenter.Distance(Point(X, Y)) < FRadius;
end;

class function TLoudGameCircleBlock.Name: string;
begin
  Result := 'CIRCLE';
end;

procedure TLoudGameCircleBlock.Paint(ACanvas: TCanvas);
var
  P1, P2: TPoint;
begin
  ACanvas.Brush.Color := clAqua;
  ACanvas.Pen.Color := clWhite;
  P1 := ToGame( Point(Start, FCenter.Y + FRadius) );
  P2 := ToGame( Point(Start + Length, FCenter.Y - FRadius) );
  ACanvas.Ellipse(P1.X, P1.Y, P2.X, P2.Y);
end;

procedure TLoudGameCircleBlock.SetData(AData: string);
var
  Level: Integer;
begin
  Parse3(AData, FStart, Level, FRadius);
  FCenter := Point(FStart + FRadius, Level);
  FLength := 2 * FRadius;
end;

{ TLoudGameDiamondBlock }

class function TLoudGameDiamondBlock.Name: string;
begin
  Result := 'DIAMOND';
end;

procedure TLoudGameDiamondBlock.SetData(AData: string);
var
  Level, Radius: integer;
begin
  Parse3(AData, FStart, Level, Radius);
  FLength := 2 * Radius;
  Add(FStart, Level);
  Add(FStart + Radius, Level + Radius);
  Add(FStart + FLength, Level);
  Add(FStart + Radius, Level - Radius);
end;

{ TLoudGameElement }

constructor TLoudGameElement.Create(AGame: TLoudGame);
begin
  FGame := AGame;
end;

{ TTimerThread }

constructor TTimerThread.Create(AGame: TLoudGame);
begin
  inherited Create(True);
  FGame := AGame;
  FTimer := TMultimediaTimer.Create(FGame);
  FTimer.Enabled    := False;
  FTimer.OnTimer    := OnTimer;
  FTimer.Resolution := TIMER_INTERVAL;
  FTimer.Interval   := TIMER_INTERVAL;
end;

destructor TTimerThread.Destroy;
begin
  FTimer.Free;
  inherited Destroy;
end;

procedure TTimerThread.Execute;
begin
  FTimer.Enabled := True;
end;

function TTimerThread.GetInterval: Word;
begin
  Result := FTimer.Interval;
end;

procedure TTimerThread.OnTimer(Sender: TObject);
begin
  Synchronize(procedure
  begin
    FGame.OnTimer(Sender);
  end);
end;

initialization
  FBlockClasses := TList<TLoudGameBlockClass>.Create;
  FBlockClasses.Add(TLoudGameUpTrapBlock);
  FBlockClasses.Add(TLoudGameDownTrapBlock);
  FBlockClasses.Add(TLoudGameRectBlock);
  FBlockClasses.Add(TLoudGameCircleBlock);
  FBlockClasses.Add(TLoudGameDiamondBlock);

end.
