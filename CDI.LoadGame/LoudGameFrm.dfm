object LoudGameForm: TLoudGameForm
  Left = 0
  Top = 0
  Caption = 'LoudGameForm'
  ClientHeight = 542
  ClientWidth = 866
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 264
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
  end
  object Button2: TButton
    Left = 408
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 1
  end
  object Button3: TButton
    Left = 152
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 2
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 866
    Height = 41
    Align = alTop
    Color = clAppWorkSpace
    ParentBackground = False
    TabOrder = 3
    object btnNewGame: TButton
      Left = 1
      Top = 1
      Width = 129
      Height = 39
      Align = alLeft
      Caption = #1053#1086#1074#1072#1103' '#1080#1075#1088#1072
      TabOrder = 0
      OnClick = btnNewGameClick
    end
    object btnReset: TButton
      Left = 736
      Top = 1
      Width = 129
      Height = 39
      Align = alRight
      Caption = #1057#1073#1088#1086#1089
      TabOrder = 1
      OnClick = btnResetClick
    end
    object btnStart: TButton
      Left = 130
      Top = 1
      Width = 129
      Height = 39
      Align = alLeft
      Caption = #1053#1072#1095#1072#1090#1100
      Enabled = False
      TabOrder = 2
      OnClick = btnStartClick
    end
    object btnStop: TButton
      Left = 259
      Top = 1
      Width = 129
      Height = 39
      Align = alLeft
      Caption = #1057#1090#1086#1087
      Enabled = False
      TabOrder = 3
      OnClick = btnStopClick
    end
  end
  object CaptionPanel: TPanel
    Left = 0
    Top = 41
    Width = 866
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object CaptionLabel: TLabel
      Left = 0
      Top = 0
      Width = 866
      Height = 41
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Color = 6502166
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 13410946
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      Layout = tlCenter
      ExplicitLeft = 240
      ExplicitTop = 8
      ExplicitWidth = 31
      ExplicitHeight = 13
    end
  end
  object MainPanel: TPanel
    Left = 0
    Top = 82
    Width = 866
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 5
    object WaveAudioLevelView: TWaveAudioLevelView
      Left = 0
      Top = 0
      Width = 32
      Height = 460
      WaveAudio = LiveAudioRecorder
      Align = alLeft
      Level = 0
    end
  end
  object LiveAudioRecorder: TLiveAudioRecorder
    PCMFormat = Stereo16bit44100Hz
    BufferLength = 100
    Async = True
    OnLevel = LiveAudioRecorderLevel
    Left = 80
    Top = 488
  end
  object OpenDialog: TOpenDialog
    Filter = 'Game (*.game)|*.game'
    Left = 368
    Top = 282
  end
end
