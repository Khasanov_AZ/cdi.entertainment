unit SelectDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TSelectDialog = class(TForm)
    Label1: TLabel;
    btnCancel: TButton;
    btnOk: TButton;
    lbPlayer: TListBox;
    lbGame: TListBox;
    Label2: TLabel;
    procedure lbPlayerClick(Sender: TObject);
  private
    procedure LoadGames(AGamePath: string);
  public
  end;

  function ShowSelectDialog(ACompetitors: TStringList; AGamePath: string; var APlayer, AGame: string): boolean;

implementation

function ShowSelectDialog(ACompetitors: TStringList; AGamePath: string; var APlayer, AGame: string): boolean;
begin
  with TSelectDialog.Create(nil) do
  try
    lbPlayer.Items.Assign(ACompetitors);
    LoadGames(AGamePath);
    Result := ShowModal = mrOk;
    if Result then
    begin
      APlayer := lbPlayer.Items[lbPlayer.ItemIndex];
      AGame   := lbGame.Items[lbGame.ItemIndex];
    end;
  finally
    Free;
  end;
end;

{$R *.dfm}

procedure TSelectDialog.lbPlayerClick(Sender: TObject);
begin
  btnOK.Enabled := (lbPlayer.ItemIndex <> -1) and (lbGame.ItemIndex <> -1);
end;

procedure TSelectDialog.LoadGames(AGamePath: string);
var
  SR: TSearchRec;
begin
  lbGame.Items.Clear;
  if FindFirst(AGamePath + '\*.mid', faAnyFile, SR) = 0 then
  repeat
    lbGame.Items.Add(Copy(SR.Name, 1, Length(SR.Name) - 4));
  until FindNext(SR) <> 0;
  FindClose(SR);
end;

end.
