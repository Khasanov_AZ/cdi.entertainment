program LoudGame;

uses
  Vcl.Forms,
  LoudGameFrm in 'LoudGameFrm.pas' {LoudGameForm},
  LoudGame.Karaoke in 'LoudGame.Karaoke.pas',
  CDI.LoudGame in 'CDI.LoudGame.pas',
  SelectDlg in 'SelectDlg.pas' {SelectDialog};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TLoudGameForm, LoudGameForm);
  Application.Run;
end.
