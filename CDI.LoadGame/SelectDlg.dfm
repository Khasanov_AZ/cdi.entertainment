object SelectDialog: TSelectDialog
  Left = 0
  Top = 0
  Caption = 'SelectDialog'
  ClientHeight = 474
  ClientWidth = 520
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    520
    474)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 73
    Height = 19
    Caption = #1059#1095#1072#1089#1090#1085#1080#1082':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 263
    Top = 17
    Width = 49
    Height = 19
    Caption = #1055#1077#1089#1085#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btnCancel: TButton
    Left = 382
    Top = 416
    Width = 130
    Height = 50
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1058#1052#1045#1053#1040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 397
    ExplicitTop = 344
  end
  object btnOk: TButton
    Left = 246
    Top = 416
    Width = 130
    Height = 50
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = 261
    ExplicitTop = 344
  end
  object lbPlayer: TListBox
    Left = 8
    Top = 41
    Width = 249
    Height = 368
    Anchors = [akLeft, akTop, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 19
    ParentFont = False
    Sorted = True
    TabOrder = 2
    OnClick = lbPlayerClick
  end
  object lbGame: TListBox
    Left = 263
    Top = 41
    Width = 249
    Height = 368
    Anchors = [akLeft, akTop, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 19
    ParentFont = False
    Sorted = True
    TabOrder = 3
    OnClick = lbPlayerClick
  end
end
