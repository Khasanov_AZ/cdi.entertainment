object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 616
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object TaskListBox: TListBox
    Left = 0
    Top = 48
    Width = 201
    Height = 568
    Style = lbOwnerDrawFixed
    Align = alLeft
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ItemHeight = 32
    ParentFont = False
    TabOrder = 0
    OnDblClick = TaskListBoxDblClick
    OnDrawItem = TaskListBoxDrawItem
    ExplicitTop = 104
    ExplicitHeight = 512
  end
  object MainPanel: TPanel
    Left = 201
    Top = 48
    Width = 807
    Height = 568
    Align = alClient
    BevelOuter = bvNone
    Color = clBlack
    ParentBackground = False
    TabOrder = 1
    ExplicitTop = 104
    ExplicitHeight = 512
    object TextPanel: TPanel
      Left = 0
      Top = 56
      Width = 807
      Height = 512
      Align = alClient
      BevelOuter = bvNone
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitTop = 0
      object SongLabel: TLabel
        Left = 0
        Top = 0
        Width = 807
        Height = 271
        Align = alClient
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 8
        ExplicitHeight = 33
      end
      object AnswerPanel: TGridPanel
        Left = 0
        Top = 312
        Width = 807
        Height = 200
        Align = alBottom
        ColumnCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = AnswerPanel1
            Row = 0
          end
          item
            Column = 1
            Control = AnswerPanel2
            Row = 0
          end
          item
            Column = 0
            Control = AnswerPanel3
            Row = 1
          end
          item
            Column = 1
            Control = AnswerPanel4
            Row = 1
          end>
        RowCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        TabOrder = 0
        object AnswerPanel1: TPanel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 396
          Height = 93
          Align = alClient
          BevelKind = bkFlat
          BevelOuter = bvNone
          Color = clBlack
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
        end
        object AnswerPanel2: TPanel
          AlignWithMargins = True
          Left = 406
          Top = 4
          Width = 397
          Height = 93
          Align = alClient
          BevelKind = bkFlat
          BevelOuter = bvNone
          Color = clBlack
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 1
        end
        object AnswerPanel3: TPanel
          AlignWithMargins = True
          Left = 4
          Top = 103
          Width = 396
          Height = 93
          Align = alClient
          BevelKind = bkFlat
          BevelOuter = bvNone
          Color = clBlack
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 2
        end
        object AnswerPanel4: TPanel
          AlignWithMargins = True
          Left = 406
          Top = 103
          Width = 397
          Height = 93
          Align = alClient
          BevelKind = bkFlat
          BevelOuter = bvNone
          Color = clBlack
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 3
        end
      end
      object VariantPanel: TPanel
        Left = 0
        Top = 271
        Width = 807
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object VariantLabel: TLabel
          Left = 0
          Top = 0
          Width = 807
          Height = 41
          Align = alClient
          Alignment = taCenter
          Caption = #1042#1072#1088#1080#1072#1085#1090#1099' '#1086#1090#1074#1077#1090#1086#1074
          Color = clGreen
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          Visible = False
          ExplicitWidth = 202
          ExplicitHeight = 25
        end
      end
      object MediaPlayer: TMediaPlayer
        Left = 544
        Top = 6
        Width = 253
        Height = 30
        DoubleBuffered = True
        Visible = False
        ParentDoubleBuffered = False
        TabOrder = 2
      end
    end
    object CaptionPanel: TPanel
      Left = 0
      Top = 0
      Width = 807
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 48
      ExplicitWidth = 1008
      object FirstPlayerLabel: TLabel
        Left = 0
        Top = 0
        Width = 300
        Height = 56
        Align = alLeft
        Alignment = taCenter
        AutoSize = False
        Caption = #1055#1077#1088#1074#1099#1081' '#1080#1075#1088#1086#1082
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = False
        Layout = tlCenter
      end
      object SecondPlayerLabel: TLabel
        Left = 507
        Top = 0
        Width = 300
        Height = 56
        Align = alRight
        Alignment = taCenter
        AutoSize = False
        Caption = #1042#1090#1086#1088#1086#1081' '#1080#1075#1088#1086#1082
        Color = clBlue
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = False
        Layout = tlCenter
        ExplicitLeft = 977
      end
      object TaskLabel: TLabel
        Left = 300
        Top = 0
        Width = 207
        Height = 56
        Align = alClient
        Alignment = taCenter
        Color = clGreen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = False
        Layout = tlCenter
        ExplicitWidth = 6
        ExplicitHeight = 23
      end
    end
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Color = clAppWorkSpace
    ParentBackground = False
    TabOrder = 2
    object btnReset: TButton
      Left = 887
      Top = 1
      Width = 120
      Height = 46
      Align = alRight
      Caption = #1057#1073#1088#1086#1089
      TabOrder = 0
      OnClick = btnResetClick
    end
    object btnPlayTask: TButton
      Left = 241
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1055#1088#1086#1080#1075#1088#1072#1090#1100
      TabOrder = 1
      OnClick = btnPlayTaskClick
    end
    object btnShowVariants: TButton
      Left = 361
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1042#1072#1088#1080#1072#1085#1090#1099
      TabOrder = 2
      OnClick = btnShowVariantsClick
    end
    object btnPlayAnswer: TButton
      Left = 481
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1054#1090#1074#1077#1090
      TabOrder = 3
      OnClick = btnPlayAnswerClick
    end
    object btnNewGame: TButton
      Left = 121
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1053#1086#1074#1072#1103' '#1080#1075#1088#1072
      TabOrder = 4
      OnClick = btnNewGameClick
    end
    object btnStopPlaying: TButton
      Left = 601
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1057#1090#1086#1087
      TabOrder = 5
      OnClick = btnStopPlayingClick
    end
    object btnSelectDirectory: TButton
      Left = 1
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1072#1087#1082#1091
      TabOrder = 6
      OnClick = btnSelectDirectoryClick
    end
    object btnNext: TButton
      Left = 721
      Top = 1
      Width = 120
      Height = 46
      Align = alLeft
      Caption = #1045#1097#1077
      TabOrder = 7
      OnClick = btnNextClick
      ExplicitLeft = 714
      ExplicitTop = -4
    end
  end
end
