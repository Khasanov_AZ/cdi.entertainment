unit MainFrm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.FileCtrl, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, mmSystem, Vcl.MPlayer;

type
  TMainForm = class(TForm)
    TaskListBox: TListBox;
    MainPanel: TPanel;
    TextPanel: TPanel;
    AnswerPanel: TGridPanel;
    AnswerPanel1: TPanel;
    AnswerPanel2: TPanel;
    AnswerPanel3: TPanel;
    AnswerPanel4: TPanel;
    VariantPanel: TPanel;
    SongLabel: TLabel;
    CaptionPanel: TPanel;
    MediaPlayer: TMediaPlayer;
    TopPanel: TPanel;
    btnReset: TButton;
    btnPlayTask: TButton;
    btnShowVariants: TButton;
    btnPlayAnswer: TButton;
    btnNewGame: TButton;
    btnStopPlaying: TButton;
    FirstPlayerLabel: TLabel;
    SecondPlayerLabel: TLabel;
    TaskLabel: TLabel;
    btnSelectDirectory: TButton;
    VariantLabel: TLabel;
    btnNext: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnPlayTaskClick(Sender: TObject);
    procedure btnShowVariantsClick(Sender: TObject);
    procedure btnPlayAnswerClick(Sender: TObject);
    procedure btnNewGameClick(Sender: TObject);
    procedure TaskListBoxDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure TaskListBoxDblClick(Sender: TObject);
    procedure btnStopPlayingClick(Sender: TObject);
    procedure btnSelectDirectoryClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
  private
    FirstPlayer, SecondPlayer: string;
    TaskPath, RifmobolPath: string;
    Competitors: TStringList;
    SongText, Answers: TStringList;
    RightIndex: integer;
    FPlaying: boolean;
    FState: integer;
    FTaskIndex: integer;
    procedure ScanDir;
    procedure Initialize;
    procedure InitializeTask;
    procedure Play(AName: string);
    procedure SetState(const Value: integer);
    procedure UpdateButtons;
    procedure Pause(ATime: integer);
    procedure SetPlaying(const Value: boolean);
    procedure SetTaskIndex(const Value: integer);
    procedure RunTask;
  public
    property State: integer read FState write SetState;
    property Playing: boolean read FPlaying write SetPlaying;
    property TaskIndex: integer read FTaskIndex write SetTaskIndex;
  end;

var
  MainForm: TMainForm;

implementation

uses SelectDlg;

const
  STATE_NULL       = -1;
  STATE_READY      = 0;
  STATE_STARTED    = 1;
  STATE_READY_TASK = 2;
  STATE_PLAYED     = 3;
  STATE_VARIANTS   = 4;
  STATE_FINISHED   = 5;
  STATE_PLAYING    = 6;

{$R *.dfm}

procedure TMainForm.btnNewGameClick(Sender: TObject);
begin
  Competitors.LoadFromFile( ExtractFilePath(Application.ExeName) + 'competitors.txt', TEncoding.UTF8 );
  if ShowSelectDialog(Competitors, FirstPlayer, SecondPlayer) then
  begin
    Initialize;
    FirstPlayerLabel.Caption  := FirstPlayer;
    SecondPlayerLabel.Caption := SecondPlayer;
    State := STATE_STARTED;
  end;
end;

procedure TMainForm.btnNextClick(Sender: TObject);
begin
  TaskIndex := -1;
  State := STATE_STARTED;
end;

procedure TMainForm.btnPlayAnswerClick(Sender: TObject);
var
  P: TPanel;
begin
  Play('answer');

  P := TPanel(FindComponent('AnswerPanel' + RightIndex.ToString));
  P.Color := clGreen;

  State := STATE_FINISHED;
end;

procedure TMainForm.btnPlayTaskClick(Sender: TObject);
begin
  RunTask;
end;

procedure TMainForm.btnResetClick(Sender: TObject);
begin
  FirstPlayer := '';
  SecondPlayer := '';
  FirstPlayerLabel.Caption  := '������ �����';
  SecondPlayerLabel.Caption := '������ �����';
  TaskIndex := -1;
  State := STATE_READY;
end;

procedure TMainForm.btnSelectDirectoryClick(Sender: TObject);
var
  Dir: string;
begin
  Dir := ExtractFilePath(Application.ExeName);
  if SelectDirectory(Dir, [], 0) then
  begin
    RifmobolPath := Dir +'\';

    ScanDir;
    Initialize;

    State := STATE_READY;
  end;
end;

procedure TMainForm.btnShowVariantsClick(Sender: TObject);
begin
  VariantLabel.Show;
  Pause(2000);
  AnswerPanel1.Caption := Answers[0];
  Pause(2000);
  AnswerPanel2.Caption := Answers[1];
  Pause(2000);
  AnswerPanel3.Caption := Answers[2];
  Pause(2000);
  AnswerPanel4.Caption := Answers[3];

  State := STATE_VARIANTS;
end;

procedure TMainForm.btnStopPlayingClick(Sender: TObject);
begin
  MediaPlayer.Stop;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  RifmobolPath := 'D:\Clouds\YandexDisk\�������� ����\��������\';
  SongText := TStringList.Create;
  Answers := TStringList.Create;
  Competitors := TStringList.Create;

  State := STATE_NULL;
end;

procedure TMainForm.Initialize;
begin
  TaskIndex := -1;

  State := STATE_READY;
end;

procedure TMainForm.InitializeTask;
var
  Task: string;
  I: integer;
begin
  VariantLabel.Hide;
  RightIndex := -1;

  SongLabel.Caption := '';
  TaskLabel.Caption := '';
  AnswerPanel1.Caption := '';
  AnswerPanel2.Caption := '';
  AnswerPanel3.Caption := '';
  AnswerPanel4.Caption := '';
  AnswerPanel1.Color := clBlack;
  AnswerPanel2.Color := clBlack;
  AnswerPanel3.Color := clBlack;
  AnswerPanel4.Color := clBlack;

  if TaskIndex = -1 then
    Exit;

  Task := TaskListBox.Items[TaskIndex];
  TaskPath := RifmobolPath + Task + '\';
  TaskLabel.Caption := '������� �' + (TaskIndex + 1).ToString;

  SongText.LoadFromFile(TaskPath + 'text.txt');
  Answers.LoadFromFile(TaskPath + 'answers.txt');
  for I := 0 to 3 do
    if Answers[I][1] = '+' then
    begin
      Answers[I] := Copy(Answers[I], 2, 100);
      RightIndex := I + 1;
      Break;
    end;

  State := STATE_READY_TASK;
end;

procedure TMainForm.Pause(ATime: integer);
var
  Ticks: cardinal;
begin
  Ticks := GetTickCount;
  while GetTickCount - Ticks < ATime do
    Application.ProcessMessages;
end;

procedure TMainForm.Play(AName: string);
begin
  MediaPlayer.FileName := TaskPath + AName + '.mp3';
  MediaPlayer.Open;
  MediaPlayer.Play;

  Playing := True;
  while MediaPlayer.Mode = mpPlaying do
    Application.ProcessMessages;
  Playing := False;
end;

procedure TMainForm.ScanDir;
var
  SR: TSearchRec;
begin
  TaskListBox.Items.Clear;
  if FindFirst(RifmobolPath + '*', faDirectory, SR) = 0 then
  repeat
    if (SR.Name <> '.') and (SR.Name <> '..') then
      TaskListBox.Items.Add(SR.Name);
  until FindNext(SR) <> 0;
  FindClose(SR);
end;

procedure TMainForm.SetPlaying(const Value: boolean);
begin
  FPlaying := Value;
  UpdateButtons;
end;

procedure TMainForm.SetState(const Value: integer);
begin
  FState := Value;
  UpdateButtons;
end;

procedure TMainForm.SetTaskIndex(const Value: integer);
begin
  FTaskIndex := Value;
  InitializeTask;
end;

procedure TMainForm.RunTask;
begin
  Play('task');
  SongLabel.Caption := SongText.Text;
  State := STATE_PLAYED;
end;

procedure TMainForm.TaskListBoxDblClick(Sender: TObject);
begin
  if State = STATE_STARTED then
  begin
    TaskIndex := TaskListBox.ItemIndex;
    RunTask;
  end;
end;

procedure TMainForm.TaskListBoxDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  R: TRect;
  T: string;
begin
  R := Rect;
  T := '������� �' + (Index + 1).ToString;
  if Index = TaskIndex then
    TListBox(Control).Canvas.Brush.Color := clMaroon
  else
    TListBox(Control).Canvas.Brush.Color := clBlack;

  TListBox(Control).Canvas.FillRect(R);
  TListBox(Control).Canvas.TextRect(R, T, [tfCenter, tfVerticalCenter, tfSingleLine]);
end;

procedure TMainForm.UpdateButtons;
begin
  btnSelectDirectory.Enabled := (State = STATE_NULL) or (State = STATE_READY);
  btnNewGame.Enabled      := not Playing and (State = STATE_READY);
  btnPlayTask.Enabled     := not Playing and (State = STATE_PLAYED);
  btnShowVariants.Enabled := not Playing and (State = STATE_PLAYED);
  btnPlayAnswer.Enabled   := not Playing and (State = STATE_VARIANTS);
  btnStopPlaying.Enabled  := Playing;
  btnReset.Enabled        := (not Playing) and (State <> STATE_NULL) and (State <> STATE_READY);
  btnNext.Enabled         := not Playing and (State = STATE_FINISHED);
end;

end.
